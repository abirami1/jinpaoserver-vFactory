<?php
	include('config.php');
	
	$workcenter = 'W01';
	$process = array();
	$machines = array();$machineWorkload = array(); $utilization = array();
	$moCount = array(); $m = 1;
	$monumber_mac = array();
	$logFile = "log/vfactory_NCT.txt";
	
	function logToFile($filename, $msg)
	{   
	   $fd = fopen($filename, "a");
	   $msg = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $msg);
	   $str = "[".date("Y/m/d h:i:s")."] ".$msg;
	   fwrite($fd, "\n".$str."\n");
	   fclose($fd);
	}

	$result = mysqli_query($conn, "SELECT m.`id`, m.`name`, m.`mid` FROM `machines` m JOIN `workcenters` w ".
				"ON m.`workcenter` = w.`id` WHERE w.`name` = '$workcenter' ORDER BY m.`mid` ASC") or die("Error: ".mysqli_error($conn));
	if(mysqli_num_rows($result) > 0)
	{
		while($mdata = mysqli_fetch_assoc($result))
		{
			$row = array();
			$row['id'] = $mdata['id'];
			$row['code'] = $mdata['mid'];
			$row['name'] = $mdata['name'];
			$row['index'] = $m;
			
			$machineWorkload[$m] = 0; $utilization[$m] = 0;
			$moCount[$m] = 0; $machines[$m] = $row;
			$m++;
		}
	}
	
	// $process['mcount'] = --$m;
	// $process['machine'] = $machines;
	
	$today = date('Ymd');
	
	$mtoday = date("Y-m-d", strtotime($today));
	$tvquery = "SELECT mp.`id`, mp.`mo_type`, mp.`mo_number`, mp.`seq`, mp.`dynamic_man_hour`, ".
			   "tv.`plan_qty` FROM `tv_plan_store` tv JOIN `mo_process_store` mp ON mp.`mo_type` = tv.`mo_type`".
			   " AND mp.`mo_number` = tv.`mo_number` AND mp.`seq` = tv.`seq` ".
			   "WHERE tv.`plan_date` = '$today' AND tv.`wc` = '$workcenter'"; 
	
	$tvresult = mysqli_query($conn, $tvquery) or die("Error: ".mysqli_error($conn));
	
	$machine = null;
	if(mysqli_num_rows($tvresult) > 0)
	{
		while($data = mysqli_fetch_array($tvresult))
		{
			$moType = $data['mo_type']; $moNo = $data['mo_number']; $moSeq = $data['seq'];
			$machineHour = $data['dynamic_man_hour']; $planQty = $data['plan_qty'];
			//$pid = $data['id'];
			
			$puquery = "SELECT * FROM `process_updates` WHERE ".
					   "`mo_type` = '$moType' AND `mo_number` = '$moNo' AND ".
					   "`seq` = '$moSeq' AND `delete` = 0 AND (`start_time` LIKE '%$mtoday%' OR `start_time` IS NULL)";
						
			$puresult = mysqli_query($conn, $puquery) or die("Error: ".mysqli_error($conn));
			
			if(mysqli_num_rows($puresult) > 0)
			{
				while($pudata = mysqli_fetch_array($puresult))
				{
					$machine = trim($pudata['machine']);					
					
					if($machine == NULL)
						continue;
					
					$neededObjects = array_filter(
						$machines,
						function ($e) {
							global $machine;
							return $e['id'] == $machine;
						}
					);
			
					if(sizeof($neededObjects) > 0)
					{
						foreach ($neededObjects as $obj)
						{
							$mindex = $obj['index'];	
							$mvalue = $planQty *$machineHour;
							$moCount[$mindex] += 1;
							$machineWorkload[$mindex] += $mvalue;
							$monumber_mac[$mindex] =$moType . $moNo;
								
						}
					}
				}
			}
		}
	}
	
	
	
	$mresult = mysqli_query($conn, "SELECT * FROM `defaults` WHERE `tag` = 'BH'") or die("Error: ".mysqli_error($conn));
	
	if(mysqli_num_rows($mresult) > 0)
	{
		$mdata = mysqli_fetch_assoc($mresult);
		$basehour = 3600 * $mdata['value'];
	}
	else
	{
		$basehour = 1;
	}
	mysqli_close($conn);
	
	$m = 1;
	foreach ($machineWorkload as $value) {	

	  $wvalue = ($value / $basehour) * 100;
	  $machineWorkload[$m] = number_format((float)$wvalue, 2, '.', '')	;
	  
	  $process[$m]['code'] = $machines[$m]['code'];
	  $process[$m]['name'] = $machines[$m]['name'];
	  $process[$m]['wc'] = $workcenter;
	  
	  $process[$m]['utilization'] = $utilization[$m];
	  $process[$m]['workload'] =  $machineWorkload[$m];
	  $process[$m]['mocount'] = $moCount[$m];
	  $process[$m]['monumber'] = $monumber_mac[$m];
	  
	  $m++;
	}
	
	$data = json_encode($process);
	
	$url = 'http://alfadock-pro.com/alfadockpro/services/layout_graph.php';
	//http://alfadock-pro.com/alfadockpro/services/layout_graph.php
	$myvars = 'data=' .$data;

	$ch = curl_init( $url );
	curl_setopt( $ch, CURLOPT_POST, 1);
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt( $ch, CURLOPT_HEADER, 0);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec( $ch );
	
	$str = "Workload: ".$data. ", \nResponse: ".$response;
	logToFile($logFile, $str);
	
?>