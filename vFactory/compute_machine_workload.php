<?php
	include('config.php');
	$today = date('Ymd');

	$machineWorkload = array(); $machines = array();
	
	$wc = 'w53';
	$machineId = 71;
	$mac = array();
	$mac['id'] = $machineId;
	$mac['index'] = 0;
	$machines[0] = $mac;
	$machineWorkload[0] = 0;
	
	$mtoday = date("Y-m-d", strtotime($today));
	$tvquery = "SELECT mp.`id`, mp.`mo_type`, mp.`mo_number`, mp.`seq`, mp.`dynamic_man_hour`, ".
			   "tv.`plan_qty` FROM `tv_plan_store` tv JOIN `mo_process_store` mp ON mp.`mo_type` = tv.`mo_type`".
			   " AND mp.`mo_number` = tv.`mo_number` AND mp.`seq` = tv.`seq` ".
			   "WHERE tv.`plan_date` = '$mtoday' AND tv.`wc` = '$wc'"; 
	
	$tvresult = mysql_query($tvquery) or die("Error: ".mysql_error());
	
	$machine = null;
	if(mysql_num_rows($tvresult) > 0)
	{
		while($data = mysql_fetch_array($tvresult))
		{
			$moType = $data['mo_type']; $moNo = $data['mo_number']; $moSeq = $data['seq'];
			$machineHour = $data['dynamic_man_hour']; $planQty = $data['plan_qty'];
			
			$puquery = "SELECT * FROM `process_updates` WHERE ".
					   "`mo_type` = '$moType' AND `mo_number` = '$moNo' AND ".
					   "`seq` = '$moSeq' AND `delete` = 0 AND ".
					   "(`start_time` LIKE '%$mtoday%' OR `start_time` IS NULL)";
						
			$puresult = mysql_query($puquery) or die("Error: ".mysql_error());
			
			if(mysql_num_rows($puresult) > 0)
			{
				while($pudata = mysql_fetch_array($puresult))
				{
					$machine = trim($pudata['machine']);					
					
					if($machine == NULL)
						continue;
					
					$neededObjects = array_filter(
						$machines,
						function ($e) {
							global $machine;
							return $e['id'] == $machine;
						}
					);
			
					if(sizeof($neededObjects) > 0)
					{
						foreach ($neededObjects as $obj)
						{
							$mindex = $obj['index'];	
							$mvalue = $planQty *$machineHour;
							$machineWorkload[$mindex] += $mvalue;
						}
					}
				}
			}
		}
	}

	
	$m = 1;
	foreach ($machineWorkload as $value) {		
	  $machineWorkload[$m] = ($value /(8.5 * 3600)) * 100;
	  $m++;
	}
?>