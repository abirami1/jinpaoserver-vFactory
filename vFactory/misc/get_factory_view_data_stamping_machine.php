<?php
	include('config.php');
	
	$data = array();
	$entities; $machines = array(); $machineWorkload = array();
	$moCount = array();
	$machineUtilization = 0; $index = 1; $wc = 'w12';
	
	$currentDate = date("d_m_Y", strtotime('0 days'));
	$hour = date("H", strtotime('0 days'));
	$minute = date("i", strtotime('0 days'));
	
	if($hour >= 7 && $hour<= 19)
		$shift = "Day Shift";
	else
		$shift = "Night Shift";
	$unit = "Stamping Machine";
	
	$directory = "//192.168.50.5\\Machinen\\Data Logger\\".
					$unit."\\".$shift."\\";
	
	//$directory = '\\\\192.168.50.5\Machinen\Data Logger\Stamping Machine';
					
	$files = scandir($directory);
	
	$machineCodes = array();
	foreach($files as $file)
	{
		if ($file === '.' or $file === '..') 
			continue;
		if(is_dir($directory.$file) && strcmp($file, "Log") != 0)
			array_push($machineCodes, $file);
	}
	
	if($minute >= 30)
		$dataFile = $hour.".30.00.txt";
	else
		$dataFile = $hour.".00.00.txt";

	for($m = 0; $m<count($machineCodes); $m++)
	{
		$dataFolder = "//192.168.50.5\\Machinen\\Data Logger\\".
					$unit."\\".$shift."\\".$machineCodes[$m]."\\".$currentDate."\\".$dataFile;
		$line = '';

		try
		{
			if ( !file_exists($dataFolder) ) {
				throw new Exception('File not found.');
			}

			$f = fopen($dataFolder, 'r');
			if ( !$f ) {
				throw new Exception('File open failed.');
			} 
		  
			$cursor = -1;

			fseek($f, $cursor, SEEK_END);
			$char = fgetc($f);

			$row = array();
			while ($char === "\n" || $char === "\r") {
				fseek($f, $cursor--, SEEK_END);
				$char = fgetc($f);
			}
			
			while ($char !== false && $char !== "\n" && $char !== "\r") {
					 
				$line = $char . $line;
				fseek($f, $cursor--, SEEK_END);
				$char = fgetc($f);
			}

			$row['code'] = $machineCodes[$m];

			$query = "SELECT  m.`id` AS mindex, m.`name` AS mname, w.`name` AS wname FROM ".
					"`machines` m JOIN `workcenters` w ".
					"ON m.`workcenter` = w.`id` WHERE `code` = '$machineCodes[$m]'".
					" AND w.`name` = '$wc'";
			$result = mysqli_query($conn, $query) or die("Error: ".mysqli_error($conn));
			
			if(mysqli_num_rows($result) > 0)
			{
				$mdata = mysqli_fetch_assoc($result);
				$row['name'] = $mdata['mname'];
				$wc = $mdata['wname'];
				$row['wc'] = $wc;
				
				$mindex = $mdata['mindex'];
				$mac = array();
				$mac['id'] = $mindex;
				$mac['index'] = $m;
				
				$machineWorkload[$m] = 0;
				$moCount[$m] = 0;
				$machines[$m] = $mac;
			}
			else{
				$row['name'] = '';
				$row['wc'] = '';
				$row['workload'] = "";
				$row['mocount'] = "";
				$machineWorkload[$m] = 0;
				$moCount[$m] = 0;
			}
			
			$entities = explode(",", $line);
			
			$time = $hour.":".$minute;
			$row['time'] = $time;
			
			$machineUtilization = $entities[8];
			$row['utilization'] = $machineUtilization;
			
			$data[$index] = $row;
			$index++;
		}
		catch(Exception $e)
		{
			$machineWorkload[$m] = 0;
			$moCount[$m] = 0;
			$row = array();
			$row['code'] = $machineCodes[$m];
			$row['name'] = ''; $row['wc'] = $wc;
			$row['time'] = "";
			$row['utilization'] = ""; $row['workload'] = "";
			$row['mocount'] = "";
			
			$data[$index] = $row;
			$index++;
			
		}
	}
	
    $today = date('Ymd');
	if(count($machines) > 0)
	{			
		$mtoday = date("Y-m-d", strtotime($today));
		$tvquery = "SELECT mp.`id`, mp.`mo_type`, mp.`mo_number`, mp.`seq`, mp.`dynamic_man_hour`, ".
				   "tv.`plan_qty` FROM `tv_plan_store` tv JOIN `mo_process_store` mp ON mp.`mo_type` = tv.`mo_type`".
				   " AND mp.`mo_number` = tv.`mo_number` AND mp.`seq` = tv.`seq` ".
				   "WHERE tv.`plan_date` = '$mtoday' AND tv.`wc` = '$wc'"; 
		
		$tvresult = mysqli_query($conn, $tvquery) or die("Error: ".mysqli_error($conn));
		
		$machine = null;
		if(mysqli_num_rows($tvresult) > 0)
		{
			while($tvdata = mysqli_fetch_array($tvresult))
			{
				$moType = $tvdata['mo_type']; $moNo = $tvdata['mo_number']; $moSeq = $tvdata['seq'];
				$machineHour = $tvdata['dynamic_man_hour']; $planQty = $tvdata['plan_qty'];
				
				$puquery = "SELECT * FROM `process_updates` WHERE ".
						   "`mo_type` = '$moType' AND `mo_number` = '$moNo' AND ".
						   "`seq` = '$moSeq' AND `delete` = 0 AND ".
						   "(`start_time` LIKE '%$mtoday%' OR `start_time` IS NULL)";
							
				$puresult = mysqli_query($conn, $puquery) or die("Error: ".mysqli_error($conn));
				
				if(mysqli_num_rows($puresult) > 0)
				{
					while($pudata = mysqli_fetch_array($puresult))
					{
						$machine = trim($pudata['machine']);					
						
						if($machine == NULL)
							continue;
						
						$neededObjects = array_filter(
							$machines,
							function ($e) {
								global $machine;
								return $e['id'] == $machine;
							}
						);
				
						if(sizeof($neededObjects) > 0)
						{
							foreach ($neededObjects as $obj)
							{
								$mindex = $obj['index'];	
								$mvalue = $planQty *$machineHour;
								$moCount[$mindex] += 1;
								$machineWorkload[$mindex] += $mvalue;
							}
						}
					}
				}
			}
		}

		
		$m = 1; 
		foreach ($machineWorkload as $value) {		
		  $wvalue = ($value /(8.5 * 3600)) * 100;
		  $machineWorkload[$m] = number_format((float)$wvalue, 2, '.', '');
		  
		  $data[$m]['workload'] = number_format((float)$wvalue, 2, '.', '');
		  
		  $data[$m]['mocount'] = $moCount[$m];
		  $m++;
		}
	}
	
	mysqli_close($conn);
	echo $vdata = json_encode($data);
	
	// $url = 'http://alfadock-pro.com/alfadockpro/services/get_layout_graph.php';
	// $myvars = 'data=' .$vdata;

	// $ch = curl_init( $url );
	// curl_setopt( $ch, CURLOPT_POST, 1);
	// curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
	// curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
	// curl_setopt( $ch, CURLOPT_HEADER, 0);
	// curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

	// $response = curl_exec( $ch );
	
	// $logFile = "log/vfactory_stamping.txt";
	
	// function logToFile($filename, $msg)
	// {   
	   // $fd = fopen($filename, "a");
	   // $msg = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $msg);
	   // $str = "[".date("Y/m/d h:i:s")."] ".$msg;
	   // fwrite($fd, "\n".$str."\n");
	   // fclose($fd);
	// }
	
	// $str = "Data: ".$vdata. ", \nResponse: ".$response;
	// logToFile($logFile, $str);
	
?>