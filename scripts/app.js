﻿
var global = new Object();

var app = angular.module('ProjectTrackingModule', ['datatables', 'ngResource','ngRoute', 'ui.bootstrap', 'angularModalService','ngAnimate','chart.js']);
app.config(function ($routeProvider) {
     var resolveProjects = {
    projects: function (Projects) {
      return Projects.fetch();
    }
  };
    
    $routeProvider
        
        .when("/home", {
            templateUrl: "home_1.html",
            controller:"HomeController"
        })
        .when("/simyhome", {
            templateUrl: "simy_home.html",
            controller:"HomeController"
        })
        .when("/dashboard", {
            templateUrl: "dashboard.html",
            controller:"DashboardController"
        })
        .when("/orders", {
        templateUrl: "orders.html",
        controller: "OrdersController"
        })
        .when("/neworder", {
            templateUrl: "new_order.html",
            controller: "NewOrderController"
        })
        .when("/edit/:id", {
            
            templateUrl:"edit_order.html",
            controller:"EditProjectController"
            
        })
        .when("/settings", {
            
            templateUrl:"settings.html",
            controller:"SettingsController"
            
        })
		.when("/adsocket", {
            
            templateUrl:"ad_socket.html",
            controller:"AdSocketController"
            
        })
        .when("/yoshimi_adsocket", {
            
            templateUrl:"yoshimi_adsocket.html",
            controller:"AdSocketController"
            
        })
        .when("/hirano_adsocket", {
            
            templateUrl:"hirano_adsocket.html",
            controller:"AdSocketController"
            
        })
        .when("/sakae_adsocket", {
            
            templateUrl:"sakae_adsocket.html",
            controller:"AdSocketController"
            
        })
        .when("/erp", {
            
            templateUrl:"erp.html",
            controller:"ErpController"    
        })
        .when("/erp-socket", {
            
            templateUrl:"erp_socket.html",
            controller:"ErpDataController"    
        })
        .when("/mo-socket", {
            
            templateUrl:"erp_data.html",
            controller:"MOSocketController"    
        })
        .when("/factory", {
            
            templateUrl:"factory.html",
            controller:"FactoryController"    
        })

        .when("/horii_factory", {
            
            templateUrl:"horii_factory.html",
            controller:"FactoryController"    
        })
        .when("/sacs_factory", {
            
            templateUrl:"sacs_factory.html",
            controller:"FactoryController"    
        })
        .when("/washin_factory", {
            
            templateUrl:"washin_factory.html",
            controller:"FactoryController"    
        })
        .when("/office", {
            
            templateUrl:"office.html",
            controller:"FactoryOfficeController" 
        })
        .when("/draw_manager", {
            
            templateUrl:"draw_manager.html",
            controller:"DrawManagerController" 
        })
        .when("/mba", {
            
            templateUrl:"mba.html",
            controller:"FactoryMBAController" 
        })
		.when("/hirano_factory", {
            
            templateUrl:"hirano_factory.html",
            controller:"HiranoFactoryController"    
        })
        .when("/sasaki_factory", {
            
            templateUrl:"sasaki_factory.html",
            controller:"SasakiFactoryController"    
        })
        .when("/nishiyama_factory", {
            
            templateUrl:"nishiyama_factory.html",
            controller:"NishiyamaFactoryController"    
        })
        .when("/atkg_factory", {
            
            templateUrl:"atkg_factory.html",
            controller:"atkgFactoryController"    
        })
        .when("/okabe_factory", {
            
            templateUrl:"okabe_factory.html",
            controller:"FactoryController"    
        })
        .when("/test_factory", {
            
            templateUrl:"test_factory.html",
            controller:"TestFactoryController"    
        })
        .when("/kimpou_factory", {
            
            templateUrl:"kimpou_factory.html",
            controller:"FactoryController"    
        })
	.when("/mori_factory", {
            
            templateUrl:"mori_factory.html",
            controller:"FactoryController"    
        })
        .when("/atkg/insroom", {
            
            templateUrl:"atkg_ins_room.html",
            controller:"AtkgInsRoom"    
        })
        .when("/yoshimi_factory", {
            
            templateUrl:"yoshimi_factory.html",
            controller:"YoshimiFactoryController"    
        })
         .when("/tsukata_factory", {
            
            templateUrl:"tsukata_factory.html",
            controller:"FactoryController"    
        })
          .when("/sakae_factory", {
            
            templateUrl:"sakae_factory.html",
            controller:"SakaeFactoryController"    
        })
         .when("/usui_factory", {
            
            templateUrl:"usui_factory.html",
            controller:"UsuiFactoryController"    
        })
         .when("/hoshiba_factory", {
            
            templateUrl:"hoshiba_factory.html",
            controller:"FactoryController"    
        })
	.when("/makino_factory", {
            
            templateUrl:"makino_factory.html",
            controller:"FactoryController"    
        })
		.when("/cad2d", {
            
            templateUrl:"cad2d.html",
            controller:"Cad2dController"    
        })
        .when("/cad", {
            
            templateUrl:"cad.html",
            controller:"CadController"    
        })
        .when("/ap100setup", {
            
            templateUrl:"ap100_setup.html",
            controller:"Ap100SetupController"    
        })
        .when("/ap100", {
            
            templateUrl:"ap100.html",
            controller:"Ap100Controller"    
        })
        .when("/scan", {
            
            templateUrl:"scan.html",
            controller:"ScanController"    
        })
        .when("/adsocdash", {
            
            templateUrl:"adsoc_dashboard.html",
            controller:"ADsocDashController"    
        })
        .when("/camera", {
            
            templateUrl:"camera.html",
            controller:"CameraController"    
        })
        .when("/inspsocket", {
            
            templateUrl:"insp_socket.html",
            controller:"InspSocketController"    
        })
        .when("/routersocket", {
            
            templateUrl:"router.html",
            controller:"RouterSheetController"    
        })
        .when("/airport", {
            
            templateUrl:"airport.html",
            controller:"AirportController"    
        }) 
        .when("/simy_airport", {
            
            templateUrl:"simy_airport.html",
            controller:"AirportController"    
        }) 
        .when("/por", {
            
            templateUrl:"por.html",
            controller:"PORController"    
        })
		.when("/erp-por", {
            
            templateUrl:"erp_por.html",
            controller:"ErpPorController"    
        })
        .when("/rod", {
            
            templateUrl:"rod.html",
            controller:"RODController"    
        })
         .when("/shared", {
            
            templateUrl:"shared.html",
            controller:"SharedController"    
        })
         .when("/airport_settings", {
            
            templateUrl:"airport_settings.html",
            controller:"AirportSettingsController"    
        })
          .when("/airport_shared_orders", {
            
            templateUrl:"airport_shared_orders.html",
            controller:"AirportSharedOrdersController"    
        })
          .when("/j_airport_shared_orders", {
            
            templateUrl:"j_airport_shared_orders.html",
            controller:"AirportSharedOrdersController"    
        })
          .when("/shared_orderslist/:id", {
            
            templateUrl:"shared_orders.html",
            controller:"SharedOrdersListController"    
        })
          .when("/airport_shared_office", {
            
            templateUrl:"airport_shared_office.html",
            controller:"AirportSharedOfficeController"    
        })
          .when("/j_airport_shared_office", {
            
            templateUrl:"j_airport_shared_office.html",
            controller:"AirportSharedOfficeController"    
        })
           .when("/simy_shared_office", {
            
            templateUrl:"simy_shared_office.html",
            controller:"AirportSharedOfficeController"    
        })
            .when("/simy_shared_orders", {
            
            templateUrl:"simy_shared_orders.html",
            controller:"AirportSharedOrdersController"    
        })
           .when("/nishiyama_shared_office", {
            
            templateUrl:"nishiyama_shared_office.html",
            controller:"AirportSharedOfficeController"    
        })
            .when("/nishiyama_shared_orders", {
            
            templateUrl:"nishiyama_shared_orders.html",
            controller:"AirportSharedOrdersController"    
        })
          .when("/shared_office/:id", {
            
            templateUrl:"shared_office.html",
            controller:"SharedOfficeController"    
        })
          .when("/insroom", {
            
            templateUrl:"ins_room.html",
            controller:"ErpControllerInsRoom"    
        })
		.when("/hirano/insroom", {
            
            templateUrl:"hirano_ins_room.html",
            controller:"HiranoInsRoom"    
        })
        .when("/sasaki/insroom", {
            
            templateUrl:"sasaki_ins_room.html",
            controller:"SasakiInsRoom"    
        })
        .when("/weld", {
            
            templateUrl:"weld.html",
            controller:"ErpControllerWeld"    
        })
        .when("/bend", {
            
            templateUrl:"bend.html",
            controller:"ErpControllerBend"    
        })
        .when("/aiquote", {
            
            templateUrl:"aiquote.html",
            controller:"AiquoteController"    
        })
        .when("/bend_order/:id/:pdtno/:cid", {
            
            templateUrl:"bend_order.html",
            controller:"BendOrderController"
        })
        .when("/weld_order/:id/:pdtno/:cid", {
            
            templateUrl:"weld_order.html",
            controller:"WeldOrderController"
        })
        .when("/aiquote_order/:id/:pdtno/:cid", {
            
            templateUrl:"aiquote_order.html",
            controller:"AIQuoteOrderController"
        })
        .when("/insp_order/:id/:pdtno/:cid", {
            
            templateUrl:"insp_order.html",
            controller:"InspOrderController"
        })
        .when("/nishiyama/weld", {
            
            templateUrl:"weld.html",
            controller:"ErpControllerWeld"    
        })
        .when("/nishiyama/bend", {
            
            templateUrl:"nishiyama_bend.html",
            controller:"ErpControllerBend"    
        })
        .when("/jinpao_factory", {
            
            templateUrl:"jinpao_factory.html",
            controller:"JinpaoFactoryController"    
        })
        .when("/jinpao/weld", {
            
            templateUrl:"jinpao_weld.html",
            controller:"ErpControllerWeld"    
        })
        .when("/attendance", {
            
            templateUrl:"factory_presence.html",
            controller:"FactoryPresenceController"    
        })
         .when("/simy_factory", {
            
            templateUrl:"simy_factory.html",
            controller:"SimyFactoryController"    
        })
        .when("/factory_attendance/:fact_id", {
            
            templateUrl:"people_presence.html",
            controller:"FactoryPeoplePresenceController"    
        })
        .when("/factory_attendance/subunit/:fact_id", {
            
            templateUrl:"people_presence.html",
            controller:"FactorySubunitPeoplePresenceController"    
        })
        .when("/weld_photo/:stime/:etime/:oid/:pid/:pdtno", {
            
            templateUrl:"weld_photo.html",
            controller:"WeldPhotoController"    
        })
        .when("/ins_photo/:stime/:etime/:oid/:pid/:pdtno", {
            
            templateUrl:"weld_photo.html",
            controller:"InsPhotoController"    
        })
        .when("/jinpao_firstfactory", {
            
            templateUrl:"jinpao_1fact.html",
            controller:"JinpaoFirstFactoryController"    
        })
		.when("/hiranoext", {
            
            templateUrl:"hirano_ext.html",
            controller:"HiranoExtFactoryController"    
        })
        .otherwise({redirectTo:"/home"})
});

  
app.controller('CustomController', ['$scope', '$element', 'close', 
  function($scope, $element, close) {

  $scope.status = null;
  $scope.note = null;

  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
 	  close({
      status: $scope.status,
      note: $scope.note
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Manually hide the modal.
    $element.modal('hide');
    
    //  Now call close, returning control to the caller.
    close({
      status: $scope.status,
      note: $scope.note
    }, 500); // close, but give 500ms for bootstrap to animate
  };


}]);

