
var app = angular.module('ProjectTrackingModule');

app.controller('MainController', ['$scope', '$http', '$window','$rootScope', '$location', function($scope, $http, $window,$rootScope,$location) {

    $http.get("services/get_operator.php")
        .then(function(response) { 
            var users = response.data;
            var guest = {worker_name : "Guest"};
            $scope.users = [];
            $scope.users = users;
            $scope.users.unshift(guest);
            $scope.set_user();
        });

    $scope.getkey = function(nameKey, users)
    {
        for (var i=0; i < users.length; i++) 
        {
            if (users[i].worker_name === nameKey) 
            {
                return i;
            }
        }
    }

    $scope.update_user = function()
    {
        localStorage.setItem("alfadock_user", JSON.stringify($scope.m_user));
        $scope.setuser_global();
    }
    $scope.setuser_global = function()
    {
        if (typeof $scope.m_user != 'undefined')
            {
                if($scope.m_user.hasOwnProperty("worker_name"))
                    global.username = $scope.m_user.worker_name;
                else
                    global.username = "Guest";
            }
            else
            {
                global.username = "Guest";

                if($scope.users.length <= 0)
                {
                    var guest = {worker_name : "Guest"};
                    $scope.users.unshift(guest);
                }
                $scope.m_user = $scope.users[0];
                localStorage.setItem("alfadock_user", JSON.stringify($scope.m_user));
            }   

    }
    $scope.set_user = function()
    {
        var user = JSON.parse(localStorage.getItem("alfadock_user"));
        var nameKey = "Guest";
        if(user != null) 
        {
            nameKey =  user.worker_name;
        }
        var sel = $scope.getkey(nameKey,$scope.users);
        $scope.m_user = $scope.users[sel];
        $scope.setuser_global();
    }
	var userLang = navigator.language || navigator.userLanguage; 
	//alert ("The language is: " + userLang.substring(0,2));
	$scope.userLang = userLang.substring(0,2);

	if($scope.userLang == "ja")
		$http.get("lang/ja.json")
		.then(function(response) { 
			$scope.lang = response.data;
		});
	
	else
		$http.get("lang/en.json")
		.then(function(response) { 
		$scope.lang = response.data;
		});

	
	//$scope.username = username;
    $scope.login_check = function()
    {
        $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        $scope.home = "#home";
        $rootScope.customer = response.data;
        $rootScope.lang = $scope.lang;
        $scope.customer = response.data;
        localStorage.setItem('customer',JSON.stringify(response.data));
        //alert("status id-->" + $scope.customer.id);
        if($scope.customer.id == "0")
        {
            //$window.location.href = "login.html";
            window.location.replace("login.html");
        }
        else if($scope.customer.id == 22)
        {
            $scope.home = "#simyhome";
            var path = $location.$$path;
            if(path == "/home") path = "/simyhome";
            $location.path(path);
        }
        
    }, function errorCallback(response) {
        window.location.replace("login.html");
    });

    }
    
    
    $scope.logout = function() {
        
        $http({
        method: 'POST',
        url: 'services/logout.php'
        }).then(function successCallback(response) {
            
            resetData();
            window.location.replace("login.html");
            
        }, function errorCallback(response) {
            resetData();
            window.location.replace("login.html");
        });
        
    }

    resetData = function()
    {
         delete localStorage['cid'];
         delete localStorage['cname'];
    }
	
}]);

app.directive('dropzone', function() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {

            var uploadURL = 'services/upload.php?custid=' + global.currentOrder.customerid + 
                "&orderid=" + global.currentOrder.id +
                "&role=" + global.currentOrder.role;
            
            var config = {
                url: uploadURL,
                maxFilesize: 5000,
                paramName: "file",
                maxThumbnailFilesize: 10,
                parallelUploads: 100,
                autoProcessQueue: true,
                addRemoveLinks: true,
                removedfile: function(file) {
                        var filename = file.name;  
            				
                        // Todo : call ajax post to call remove_file.php and pass filename
                        $.post('services/remove_file.php',
						{
							filename: filename						
						},
						
						function(data, status){
							if(!status){
								//alert("please try again");
							}												
						});
						
                        var _ref;
                        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;       
                    }
            };

            var eventHandlers = {
                'addedfile': function(file) {
                    scope.file = file;
                    if (this.files[1]!=null) {
                        //this.removeFile(this.files[0]);
                    }
                    scope.$apply(function() {
                        scope.fileAdded = true;
                    });
                    
                },

                'success': function (file, response) {
                }

            };

            dropzone = new Dropzone(element[0], config);

            angular.forEach(eventHandlers, function(handler, event) {
                dropzone.on(event, handler);
            });

            scope.processDropzone = function() {
                dropzone.processQueue();
            };

            scope.resetDropzone = function() {
                dropzone.removeAllFiles();
            }
        }
    }
});

// To keep the session alive forever, the below technique is used
// {{
var refreshSn = function ()
{
    var time = 600000; // 10 mins
    setTimeout(
        function ()
        {
        $.ajax({
           url: 'services/refresh_session.php',
           cache: false,
           complete: function () {refreshSn();}
        });
    },
    time
);
};

refreshSn();
