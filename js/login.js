var app = angular.module('loginApp', []);
var isTried = false;

app.controller('MainController', function($scope, $http,$rootScope) {
    
	var userLang = navigator.language || navigator.userLanguage; 
	//alert ("The language is: " + userLang.substring(0,2));
	$scope.userLang = userLang.substring(0,2);

	

	if($scope.userLang == "ja")
		$http.get("lang/ja.json")
		.then(function(response) { 
			$scope.lang = response.data;
		});
	
	else
		$http.get("lang/en.json")
		.then(function(response) { 
		$scope.lang = response.data;
		});

		$scope.login_check = function(cid,cname)
    	{
	    	isTried = true;

	        $http({
	        method: 'POST',
	        url: 'services/login_check.php',
	        data: {id : cid, name: cname}
	        }).then(function successCallback(response) {
	        $scope.home = "#home";
	        $rootScope.customer = response.data;
	        $rootScope.lang = $scope.lang;
	        $scope.customer = response.data;
	        localStorage.setItem('customer',JSON.stringify(response.data));
	        //alert("status id-->" + $scope.customer.id);
	        if($scope.customer.id == "0")
	        {
	            //$window.location.href = "login.html";
	            //window.location.replace("login.html");

	        }
	        else if($scope.customer.id == 22)
	        {
	            $scope.home = "#simyhome";
	            var path = $location.$$path;
	            if(path == "/home") path = "/simyhome";
	            $location.path(path);
	        }
	        else
	        {
	        	window.location.replace("main.html");
	        }
	        
	    }, function errorCallback(response) {
	        //window.location.replace("login.html");
	    });

    }

	//Cookie check
	function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
	}

	try
	{
		var cid = localStorage.getItem("cid");
		var name = localStorage.getItem("cname");

		if(cid && name  && isTried == false )
		{
			$scope.login_check(cid,name);
		}
		else if( isTried == false)
		{
				$http({
		        method: 'POST',
		        url: 'services/get_cust.php'
		        }).then(function successCallback(response) {

		        $rootScope.customer = response.data;
		        $rootScope.lang = $scope.lang;
		        $scope.customer = response.data;

	        	if($scope.customer.id != "0")
		        {
		            //$window.location.href = "login.html";
		            //window.location.replace("login.html");
			        localStorage.setItem('customer',JSON.stringify(response.data));
			        //alert("status id-->" + $scope.customer.id);
			        
			        if($scope.customer.id == 22)
			        {
			            window.location.replace('main.html#/simyhome');
			        }
			        else
	       			window.location.replace('main.html');

		        }
		        else isTried== true;
		        
		        
		    }, function errorCallback(response) {
		        //window.location.replace("login.html");
		        isTried== true;
		    });
		}

	}
	catch(err)
	{

	}
	
	/*if(getCookie("custid") !== "")
	{
		window.location.replace('main.html');
	}*/	
	$scope.rememberMe = false;
	
   $scope.login = function() { 
    
        var loginData = {
				username : $scope.userName,
				password : $scope.password,
				rememberMe : $scope.rememberMe
		};
        
        var res = $http.post('services/login.php', loginData);
		res.success(function(data, status, headers, config) {

			if(data['status'] == "success")
			{
				if($scope.rememberMe == true)
				{
					localStorage.setItem("cid", data['custid']);
					localStorage.setItem("cname", $scope.userName);
				}
				

				if(data['type'] == 100)
					window.location.replace('admin/main.html');
				else if(data['custid'] == 22)
					window.location.replace('main.html#/simyhome');
				else
					window.location.replace('main.html');
			}
			else
			{
				alert("Invalid username or password");
				$('#loginname').focus();
			}
            
		});
		res.error(function(data, status, headers, config) {
			//alert( "failure message: " + JSON.stringify({data: status}));
		});	
       
   };

   
    
    
});