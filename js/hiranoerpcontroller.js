var app = angular.module('ProjectTrackingModule');
var cb1StateChanged,cb2StateChanged; 
var updateprocess,delete_order;
app.controller('HiranoFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
	
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
        $scope.secondFactory = "factory_layout/"+ $scope.customer.id +"/factory2/index.html";
      });

  $scope.$parent.navSelectedItem = "hiranofactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
		  
}]);
app.controller('HiranoInsRoom', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $scope.$parent.navSelectedItem = "inspectionroom";
  $scope.$parent.subtitle = "Inspection Room";
  $scope.$parent.pageClass = 'page-contact';

  }]);

app.controller('JinpaoFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
      });
  $scope.$parent.subtitle = "Jinpao Factory";
  $scope.$parent.navSelectedItem = "jinpaofactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";

 }]);

app.controller('JinpaoFirstFactoryController', ['$scope', function($scope) {
  
  $scope.$parent.subtitle = "Jinpao First Factory Unit";
  $scope.$parent.navSelectedItem = "jinpaofactoryunit";
  $scope.$parent.pageClass = 'page-about';
  var customer = localStorage.getItem('customer');
  customer = JSON.parse(customer);
  $scope.unit1 = "factory_layout/"+ customer.id +"/factory1/unit1/index.html";
  $scope.unit2 = "factory_layout/"+ customer.id +"/factory1/unit2/index.html";
  $scope.unit3 = "factory_layout/"+ customer.id +"/factory1/unit3/index.html";
  $scope.exterior = "factory_layout/"+ customer.id +"/factory1/exterior/index.html";
  $scope.unit4 = "factory_layout/"+ customer.id +"/factory1/unit4/index.html";
  $scope.unit5 = "factory_layout/"+ customer.id +"/factory1/unit5/index.html";
  $scope.unit6 = "factory_layout/"+ customer.id +"/factory1/unit6/index.html";

 }]);
 
 app.controller('HiranoExtFactoryController', ['$scope', function($scope) {
  
  $scope.$parent.subtitle = "Hirano Factory Exterior";
  $scope.$parent.navSelectedItem = "hirano factory ext";
  $scope.$parent.pageClass = 'page-about';
 
  $scope.unit1 = "factory_layout/1/factory1/index.html";
  $scope.unit2 = "factory_layout/1/factory2/index.html";
  $scope.unit3 = "factory_layout/1/factory3/index.html";
  
 }]);

app.controller('SimyFactoryController', ['$scope', '$http', function($scope, $http) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        //$scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
      });
  $scope.$parent.subtitle = "NY-Myanmar Factory";
  $scope.$parent.navSelectedItem = "NY-Myanmarfactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";

 }]);

app.controller('FactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " +  lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
        $scope.secondFactory = "factory_layout/"+ $scope.customer.id +"/factory2/index.html";
      });

  $scope.$parent.navSelectedItem = $scope.customer.name+ "factory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
      
}]);

var selc_date,sdate;
app.controller('FactoryPresenceController', ['$scope', '$location', '$routeParams', '$http','$filter', '$timeout', 'ModalService','$window', function($scope, $location, $routeParams, $http, $filter,$timeout,ModalService,$window) {
  $scope.$parent.navSelectedItem = "Factory Presence Indicator";
  $scope.$parent.pageClass = 'page-about';
  $scope.$parent.subtitle = "Factory Presence Indicator";

  $scope.$on('$routeChangeStart', function(next, current) { 
    clearInterval(getFactoryInterval);
 });

  $scope.getFactoryPresence = function()
  {
      selc_date = $("#datetimepicker1").find("input").val();
      sdate = new Date(selc_date);
      sdate = $scope.formatDate(sdate);

     $http({
            method: 'POST',
            data : {selc_date : sdate},
            url: 'services/get_factory_presence.php'
            }).then(function successCallback(response) {
               
               $scope.factory = response.data;
            });
  };
  
  $scope.formatDate = function(date)
  {
    return $filter('date')(date, "yyyy-MM-dd");
  }
   var dp1 = $('#datetimepicker1').datetimepicker({format: 'YYYY-MMM-DD',  defaultDate : new Date()});
        dp1.on("dp.change", function(e) {
            
            $scope.getFactoryPresence();
           
        });

   var dpFrom = $('#dtpFrom').datetimepicker({format: 'YYYY-MMM-DD',  defaultDate : new Date()});
   var dpTo = $('#dtpTo').datetimepicker({format: 'YYYY-MMM-DD',  defaultDate : new Date()});

   $scope.rfidReportGen = function()
   {
      var fromDate = $("#dtpFrom").find("input").val();
       var toDate  = $("#dtpTo").find("input").val();
      fromDate = new Date(fromDate);
      fromDate = $scope.formatDate(fromDate);

      toDate = new Date(toDate);
      toDate = $scope.formatDate(toDate);
      
      var isChecked = $('input[name=rifd_radio]').is(':checked');
      if(!isChecked)
      {
        alert("please select a factory to generate report");
        return;
      } 
      var fact_unit = $('input[name=rifd_radio]:checked').attr('unit');
      var fact_id,subunit_id;

      if(fact_unit == "sub")
      {
         subunit_id = $('input[name=rifd_radio]:checked').attr('id');
         fact_id = $('input[name=rifd_radio]:checked').attr('par_id');
      }
      else
      {
         fact_id = $('input[name=rifd_radio]:checked').attr('id');
         subunit_id = 0;
      }

      var fact_name = $('input[name=rifd_radio]:checked').attr('factname');
      var url = "rfid/report.html?fdate=" + fromDate + "&todate=" + toDate + "&fid=" + fact_id +  "&subid=" +subunit_id +  "&factname=" +fact_name;
      $window.open(url);

     /*$http({
            method: 'POST',
            data : {from_date : fromDate,to_date : toDate, fact_id : fact_id, subunit_id : subunit_id},
            url: 'services/rfid_generate_report.php'
            }).then(function successCallback(response) {
               
               alert(response.data);
               //$scope.factory = response.data;
            });*/
   }

  /*if(typeof(EventSource) !== "undefined") {
      var source = new EventSource("services/get_factory_presence.php");
      source.onmessage = function(event) {
      $scope.factory = event.data;
  };
  } else {
      alert("sse not supported");
  }*/
  var getFactoryInterval = setInterval($scope.getFactoryPresence, 300000);  // call for every 5 minutes

  $scope.getFactoryPresence();
  
}]);

app.controller('FactoryPeoplePresenceController', ['$scope', '$location', '$routeParams', '$http','$filter', 'ModalService', function($scope, $location, $routeParams, $http, $filter,ModalService) {
  $scope.factid = $routeParams.fact_id;

  $scope.$parent.navSelectedItem = "PresentWorkers";
  $scope.$parent.subtitle = "Present Workers";
  $scope.$parent.pageClass = 'page-about';
  $scope.bgcolor = "#cccccc";

   $scope.$on('$routeChangeStart', function(next, current) { 
    clearInterval(getPeopleInterval);
 });
  $scope.formatDate = function(date)
  {
    return $filter('date')(date, "yyyy-MM-dd");
  }

 $scope.getPeoplePresence = function()
  {
    selc_date = $("#datetimepicker2").find("input").val();
    sdate = new Date(selc_date);
    sdate = $scope.formatDate(sdate);

    $http({
        method: 'POST',
        data : {"factid" : $scope.factid,"sel_date" : sdate},
        url: 'services/get_worker_presence.php'
        }).then(function successCallback(response) {
           
           $scope.workers = response.data;
           $scope.selcDate = selc_date;
        });
  }
  var d = new Date();
  if(sdate !== undefined) 
    d = sdate;
  var dp1 = $('#datetimepicker2').datetimepicker({format: 'YYYY-MMM-DD',  defaultDate : new Date(d)});
        dp1.on("dp.change", function(e) {
            
            $scope.getPeoplePresence();
        });

  var getPeopleInterval = setInterval($scope.getPeoplePresence, 300000);  // call for every 5 minutes
  $scope.getPeoplePresence();
      
}]);

app.controller('FactorySubunitPeoplePresenceController', ['$scope', '$location', '$routeParams', '$http','$filter', 'ModalService', function($scope, $location, $routeParams, $http, $filter,ModalService) {
  $scope.factid = $routeParams.fact_id;

  $scope.$parent.navSelectedItem = "PresentWorkers";
  $scope.$parent.subtitle = "Present Workers";
  $scope.$parent.pageClass = 'page-about';
  $scope.bgcolor = "#cccccc";

   $scope.$on('$routeChangeStart', function(next, current) { 
    clearInterval(getPeopleInterval);
 });
  $scope.formatDate = function(date)
  {
    return $filter('date')(date, "yyyy-MM-dd");
  }

 $scope.getPeoplePresence = function()
  {
    selc_date = $("#datetimepicker2").find("input").val();
    sdate = new Date(selc_date);
    sdate = $scope.formatDate(sdate);

    $http({
        method: 'POST',
        data : {"factid" : $scope.factid,"sel_date" : sdate},
        url: 'services/get_subunit_worker_presence.php'
        }).then(function successCallback(response) {
           
           $scope.workers = response.data;
           $scope.selcDate = selc_date;
        });
  }
  var d = new Date();
  if(sdate !== undefined) 
    d = sdate;
  var dp1 = $('#datetimepicker2').datetimepicker({format: 'YYYY-MMM-DD',  defaultDate : new Date(d)});
        dp1.on("dp.change", function(e) {
            
            $scope.getPeoplePresence();
        });

  var getPeopleInterval = setInterval($scope.getPeoplePresence, 300000);  // call for every 5 minutes
  $scope.getPeoplePresence();
      
}]);

app.controller('FactoryOfficeController', ['$scope', '$location', '$routeParams', '$http', '$filter','DTOptionsBuilder', 'DTColumnBuilder','ModalService', 
  function($scope, $location, $routeParams, $http, $filter,DTOptionsBuilder, DTColumnBuilder, ModalService) {
  
   $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        $scope.customer = response.data;
        $scope.$parent.navSelectedItem = $scope.customer.name + " Office";
        $scope.$parent.subtitle = $scope.customer.name + " Office";
      });
  $scope.$parent.pageClass = 'page-about';

  $scope.checkedID = [];  //Checked checkboxes
  $scope.dtInstance = {};

  var lang = $scope.lang;
  //$("#btnRemove").attr("disabled", true);
  //$("#btnAddToOrder").attr("disabled", true);

  $scope.cb1StateChanged = function(cb)
   {
   }

  $scope.cb2StateChanged = function(cb)
       {
          //alert("clicked" + cb.checked + cb.value);
          if(cb.value === "") return;

          var request = $http({
            method: "post",
            url: 'services/update_office_share.php',
            data: { id : cb.value, status : cb.checked},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
            }); 
       };

    

   cb2StateChanged = $scope.cb2StateChanged;

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_office_files.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle(lang.select).renderWith(function(data,type,full)
   {
      var cb = '<div style="text-align:center"><input type="checkbox" id="cb1" style="width:20px; height:20px;" value='+data+ '></div>';
      return cb;
   }),
   DTColumnBuilder.newColumn('id').withTitle('ID'),
     
   DTColumnBuilder.newColumn('filename').withTitle(lang.filename).renderWith(function(data,type,full)
    {
      var ext = data.split('.').pop().toLowerCase();
      var thumbhref = full.thumbhref;
      if(thumbhref == "")
      {
          if(ext == "zip") thumbhref = "images/thumb_zip.png";
          if(ext == "dwg") thumbhref = "images/thumb_dwg.png";
          if(ext == "dxf") thumbhref = "images/thumb_dxf.png";
          if(ext == "csv") thumbhref = "images/thumb_csv.png";
          if(ext == "sldprt" || ext == "sldasm") thumbhref = "images/thumb_sw.png";
          if(thumbhref == "") thumbhref = "images/thumb_file.png"
      }
      var ret = '<a href="' + full.href + '" target="_blank"> <img height="150" width="150" style="padding:1px; border-radius: 10px; border:2.5px ridge #bfbfbf" src="'+ thumbhref  +'"></a>';
    
     // var ret = '<div style="padding:2px; border:1px ridge #ffa366">'+temp+'</div>';
      ret += '<a style="margin-left:10px" href= "' + full.href + '" download >' + data + '</a><br>';
      return ret;
    }),
   DTColumnBuilder.newColumn('user').withTitle("User"),
   DTColumnBuilder.newColumn('type').withTitle(lang.filetype),
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('date').withTitle(lang.date).renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('public').withTitle(lang.public).renderWith(function(data,type,full)
         {
            if(data == "true")
                var cb = '<div style="text-align:center"><input type="checkbox" id="cb2" style="width:20px; height:20px;" value='+full.id+ ' onclick="cb2StateChanged(this)" checked></div>';
            else
                var cb = '<div style="text-align:center"><input type="checkbox" id="cb2" style="width:20px; height:20px;" value='+full.id+ ' onclick="cb2StateChanged(this)"></div>';
             
                return cb;
              
         })
   //DTColumnBuilder.newColumn('date').withTitle('Actions'),
   ];

   $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
        
  $scope.addFile = function()
  {
        ModalService.showModal({
            templateUrl: "add_files_office.html",
            controller: "AddFilesOfficeController",
        }).then(function(modal) {
            modal.element.modal();
            //modal.order = order;
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }

                 var resetPaging = false;
                $scope.dtInstance.reloadData(callback, resetPaging);
            
                function callback(json) {
                console.log(json);
                }
            });
        });
  };

  $scope.removeFiles = function()
  {
    $scope.checkedID = [];
    $('input:checkbox').each(function () {
      if(this.id == "cb1")
         this.checked ? $scope.checkedID.push($(this).val()) : "";
      });

    if($scope.checkedID.length == 0)
    {
        alert("Please select files to remove");
        return;
    }
    if (!confirm("Are you confirm about deleting the files?")) return;
    var res = {"fdata" :$scope.checkedID};
        $http({
        method: 'POST',
        data : res,
        url: 'services/remove_office_files.php'
        }).then(function successCallback(response) {
            var resetPaging = false;
            $scope.dtInstance.reloadData(callback, resetPaging);
            
            function callback(json) {
               console.log(json);
            }
        });
  };

  $scope.addToOrder = function()
  {
      $scope.checkedID = [];
      $('input:checkbox').each(function () {
        if(this.id == "cb1")
           this.checked ? $scope.checkedID.push($(this).val()) : "";
        });
      if($scope.checkedID.length == 0)
      {
          alert("Please select files to place order");
          return;
      }

        ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
		$scope.loading = true;
                result['filesdata'] = $scope.checkedID;
                
                var request = $http({
                    method: "post",
                    url: 'services/office_neworder.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {
                  
                  //alert("You have successfully placed an order !!! Please check your orders icon & verify by your registered mailid !!!");
		$scope.loading = false;			
		$location.path("/orders");
                });
            });
        });
  };
        
}]);

app.controller('DrawManagerController', ['$scope', '$location', '$routeParams', '$http', '$filter','DTOptionsBuilder', 'DTColumnBuilder','ModalService', 
  function($scope, $location, $routeParams, $http, $filter,DTOptionsBuilder, DTColumnBuilder, ModalService) {
  
   $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        $scope.customer = response.data;
        $scope.$parent.navSelectedItem = $scope.customer.name + " DrawingManager";
        $scope.$parent.subtitle = $scope.customer.name + " Drawing Manager";
      });
  $scope.$parent.pageClass = 'page-about';

  $scope.checkedID = [];  //Checked checkboxes
  $scope.dtInstance = {};

  var lang = $scope.lang;

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_drawing_files.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
    DTColumnBuilder.newColumn('id').withTitle(lang.select).renderWith(function(data,type,full)
   {
      var cb = '<div style="text-align:center"><input type="checkbox" id="cb1" style="width:20px; height:20px;" value='+data+ '></div>';
      return cb;
   }),
   
   DTColumnBuilder.newColumn('d_no').withTitle('Drawing no'),
     
   DTColumnBuilder.newColumn('d_name').withTitle('Drawing name').renderWith(function(data,type,full)
    {
      return "<a href= " + full.href + " download >" + data + "</a><br>";
    }),
   DTColumnBuilder.newColumn('pdt_name').withTitle(lang.pdt_name),
   DTColumnBuilder.newColumn('comp_name').withTitle(lang.companyname),
   DTColumnBuilder.newColumn('cdate').withTitle("Creation date"),
   DTColumnBuilder.newColumn('mdate').withTitle("Modified date"),
   DTColumnBuilder.newColumn('comment').withTitle(lang.comments)
    ];

   $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
        
  $scope.addDrawingFiles = function()
  {
        ModalService.showModal({
            templateUrl: "add_drawing_files.html",
            controller: "AddDrawingFilesController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }

                 var resetPaging = false;
                $scope.dtInstance.reloadData(callback, resetPaging);  
                function callback(json) {
               console.log(json);
            }
            });
        });
  };
     $scope.removeFiles = function()
  {
    $scope.checkedID = [];
    $('input:checkbox').each(function () {
      if(this.id == "cb1")
         this.checked ? $scope.checkedID.push($(this).val()) : "";
      });

    if($scope.checkedID.length == 0)
    {
        alert("Please select files to remove");
        return;
    }
    if (!confirm("Are you confirm about deleting the files?")) return;
    var res = {"fdata" :$scope.checkedID};
        $http({
        method: 'POST',
        data : res,
        url: 'services/remove_drawing_files.php'
        }).then(function successCallback(response) {
            var resetPaging = false;
            $scope.dtInstance.reloadData(callback, resetPaging);
            
            function callback(json) {
               console.log(json);
            }
        });
  };   

   $scope.addToOrder = function()
  {
      $scope.checkedID = [];
      $('input:checkbox').each(function () {
        if(this.id == "cb1")
           this.checked ? $scope.checkedID.push($(this).val()) : "";
        });
      if($scope.checkedID.length == 0)
      {
          alert("Please select files to place order");
          return;
      }

        ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filesdata'] = $scope.checkedID;
                
                var request = $http({
                    method: "post",
                    url: 'services/draw_to_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {
                  
                  //alert("You have successfully placed an order !!! Please check your orders icon & verify by your registered mailid !!!");
                  $location.path("/orders");

                });
            });
        });
  };
}]);


app.controller('FactoryMBAController', ['$scope', '$location', '$routeParams', '$http', '$filter','DTOptionsBuilder', 'DTColumnBuilder','ModalService', 
  function($scope, $location, $routeParams, $http, $filter,DTOptionsBuilder, DTColumnBuilder, ModalService) {
  
   $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        $scope.customer = response.data;
        $scope.$parent.navSelectedItem = $scope.customer.name + " MBA";
        $scope.$parent.subtitle = $scope.customer.name + " MBA";
      });
  $scope.$parent.pageClass = 'page-about';

  $scope.checkedID = [];  //Checked checkboxes
  $scope.dtInstance = {};

  var lang = $scope.lang;
  //$("#btnRemove").attr("disabled", true);
  //$("#btnAddToOrder").attr("disabled", true);

  $scope.cb1StateChanged = function(cb)
       {
       }

  $scope.cb2StateChanged = function(cb)
       {
          //alert("clicked" + cb.checked + cb.value);
          if(cb.value === "") return;

          var request = $http({
            method: "post",
            url: 'services/update_office_share.php',
            data: { id : cb.value, status : cb.checked},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
            }); 
       };

   cb2StateChanged = $scope.cb2StateChanged;

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_mba_files.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle(lang.select).renderWith(function(data,type,full)
   {

      var cb = '<div style="text-align:center"><input type="checkbox" id="cb1" style="width:20px; height:20px;" value='+data+ ' onclick="cb1StateChanged(' +data +')"></div>';
      return cb;
   }),
   DTColumnBuilder.newColumn('id').withTitle('ID'),
     
   DTColumnBuilder.newColumn('filename').withTitle(lang.filename).renderWith(function(data,type,full)
    {
      return "<a href= " + full.href + " download >" + data + "</a><br>";
    }),

   DTColumnBuilder.newColumn('meeting').withTitle("Meeting"),
   DTColumnBuilder.newColumn('type').withTitle(lang.filetype),
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('date').withTitle(lang.date).renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('public').withTitle(lang.public).renderWith(function(data,type,full)
         {
            if(data == "true")
                var cb = '<div style="text-align:center"><input type="checkbox" id="cb2" style="width:20px; height:20px;" value='+full.id+ ' onclick="cb2StateChanged(this)" checked></div>';
            else
                var cb = '<div style="text-align:center"><input type="checkbox" id="cb2" style="width:20px; height:20px;" value='+full.id+ ' onclick="cb2StateChanged(this)"></div>';
             
                return cb;
              
         })
   //DTColumnBuilder.newColumn('date').withTitle('Actions'),
   ];

   $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
        
  $scope.addFile = function()
  {
        ModalService.showModal({
            templateUrl: "add_files_office.html",
            controller: "AddFilesOfficeController",
        }).then(function(modal) {
            modal.element.modal();
            //modal.order = order;
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }

                 var resetPaging = false;
                $scope.dtInstance.reloadData(callback, resetPaging);
            
                function callback(json) {
                console.log(json);
                }
            });
        });
  };

  $scope.removeFiles = function()
  {
    $scope.checkedID = [];
    $('input:checkbox').each(function () {
      if(this.id == "cb1")
         this.checked ? $scope.checkedID.push($(this).val()) : "";
      });

    if($scope.checkedID.length == 0)
    {
        alert("Please select files to remove");
        return;
    }
    if (!confirm("Are you confirm about deleting the files?")) return;
    var res = {"fdata" :$scope.checkedID};
        $http({
        method: 'POST',
        data : res,
        url: 'services/remove_mba_files.php'
        }).then(function successCallback(response) {
            var resetPaging = false;
            $scope.dtInstance.reloadData(callback, resetPaging);
            
            function callback(json) {
               console.log(json);
            }
        });
  };
        
}]);

app.controller('AddFilesOfficeController', ['$scope', '$element', 'close',
  function($scope, $element, close) {

  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  $scope.cancel = function() {

    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };


}]);

app.controller('AddDrawingFilesController', ['$scope', '$element', 'close','$http',
  function($scope, $element, close,$http) {

  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  $scope.cancel = function() {

    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

$scope.submit = function()
{
  $http.post('services/update_drawing_manager.php', 
      { 
        d_no : $scope.d_no, 
        pdt_name  : $scope.pdt_name,
        comp_name  : $scope.comp_name,
        comment   : $scope.comment
       }).success(function(response) {
                  
                    $scope.close();
                  
          });
}
}]);
app.controller('SasakiFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
      });

  $scope.$parent.navSelectedItem = "sasakifactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
        
}]);

app.controller('SakaeFactoryController', ['$scope', '$http', function($scope, $http) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        //$scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
      });

  $scope.$parent.navSelectedItem = "sakaefactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
        
}]);
app.controller('UsuiFactoryController', ['$scope', '$http', function($scope, $http) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
      });

  $scope.$parent.navSelectedItem = "usuifactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
        
}]);
app.controller('SasakiInsRoom', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $scope.$parent.navSelectedItem = "inspectionroom";
  $scope.$parent.subtitle = "Inspection Room";
  $scope.$parent.pageClass = 'page-contact';

  }]);

app.controller('NishiyamaFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
      });

  $scope.$parent.navSelectedItem = "nishiyamafactory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
        
}]);

app.controller('atkgFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = "alfaTKG " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
        $scope.secondFactory = "factory_layout/"+ $scope.customer.id +"/factory2/index.html";
      });

  $scope.$parent.navSelectedItem = "alfaTKG factory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
        
}]);

app.controller('TestFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = "Test " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
        $scope.secondFactory = "factory_layout/"+ $scope.customer.id +"/factory2/index.html";
      });

  $scope.$parent.navSelectedItem = "Test factory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
        
}]);

app.controller('atkgOfficeController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $scope.$parent.navSelectedItem = "alfaTKG Office";
  $scope.$parent.subtitle = "alfaTKG Office";
  $scope.$parent.pageClass = 'page-about';

  $(document).ready(function() {
    $('#atkgoffice').DataTable();
  } );
        
}]);
app.controller('AtkgInsRoom', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $scope.$parent.navSelectedItem = "inspectionroom";
  $scope.$parent.subtitle = "Inspection Room";
  $scope.$parent.pageClass = 'page-contact';

  }]);

app.controller('YoshimiFactoryController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        var lang = $scope.lang;
        $scope.customer = response.data;
        $scope.$parent.subtitle = $scope.customer.name + " " + lang.factory;
        $scope.firstFactory = "factory_layout/"+ $scope.customer.id +"/factory1/index.html";
        
      });

  $scope.$parent.navSelectedItem = "yoshimi factory";
  $scope.$parent.pageClass = 'page-about';
  $scope.airport = "#airport";
      
}]);

app.controller('ErpControllerInsRoom', ['$scope', '$location', '$routeParams', '$http','$rootScope', 'ModalService', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', function($scope, $location, $routeParams, $http,$rootScope, ModalService,$filter, DTOptionsBuilder, DTColumnBuilder) {

 $scope.$parent.navSelectedItem = "InspectionRoom";
  $scope.$parent.subtitle = "Inspection";
  $scope.$parent.pageClass = 'page-contact';

  var lang = $scope.lang;

  $scope.dtInstance = {};

   $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_inskiosk.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc'])
        .withOption('autoWidth', true);

        var waiting = "margin:auto; border-radius: 5px;background: #ff751a;width: 35px;height: 30px;text-align:center";

        var finished = "margin:auto; border-radius: 5px;background: #009900;width: 35px;height: 30px;text-align:center;";  

        var delay = "margin:auto; border-radius: 5px;background: #ff0000;width: 35px;height: 30px;text-align:center;";   
                     
    $scope.dtColumns = [

        DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
          var rpath = "";//'uploads/weldkiosk/' + full.cid  +'/' + data + '/Reports/' + full.pdt_no +'-PHD.pdf';
          
          var phd_show = "enabled";
          var phd_img = "images/sheet.png";
          if(full.phd != "") 
            rpath = full.phd;
            else if(full.dd != "")
              rpath = full.dd;
              else
              {
                  phd_show = "disabled";
                  phd_img = "images/sheet_cross.png";
              }

          var ret1 = '<a onclick=updateprocess(' + data + ',' + '"' + encodeURIComponent(full.pdt_no) + '"' + ')' + '><img height="32" width="32" src="images/vernier.png"></a> <a href=' + 
          "/kioskinspection/index.html?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank"><img src="images/eye.png"></a>';
          //'<a onclick="delete_order('+ data + ')"><img src="images/weld_delete.png"></a>';  
          
         // if(full.status == "finished")
            ret1 = ret1 + '<a href="' + rpath + '" target="_blank" style=\"margin-left:5px;\" class="' + phd_show + '"><img src="' + phd_img + '"></a>';
         
          return ret1;
        }),
        DTColumnBuilder.newColumn('status').withTitle(lang.status).renderWith(function(data, type, full) {
          if(data == "started") 
           return '<div style="text-align:center" ng-model=' +"status-" + full.id + '> <img height="32" width="32" src="images/ruler.png">  </div>';

         /* {
             var st = $scope.checkDate(full.comp_date, full.adj_date); 
             if(st[0] === "0")
                return '<div><div style="' + delay  +'"ng-model=' +"status_" + full.id + '></div></div>';
          } */
          
          else if(data == "waiting") 
              return '<div><div style="' + waiting  +'"ng-model=' +"status_" + full.id + '></div></div>';

            /*{
              var st = $scope.checkDate(full.comp_date, full.adj_date); 
              if(st[0] === "0")
                return '<div><div style="' + delay  +'"ng-model=' +"status_" + full.id + '></div></div>';
            }  */         
          else if(data === "finished") return '<div><div style="' + finished  +'"ng-model=' +"status_" + full.id + '></div></div>';
        }),
        DTColumnBuilder.newColumn('order_no').withTitle(lang.orderno),
        DTColumnBuilder.newColumn('pdt_no').withTitle(lang.pdt_no).renderWith(function(data, type, full) {
          var ret = full.id + "/" + data + "/" + full.cid;
          ret = encodeURIComponent(ret);
          return '<a href=#insp_order/' + ret + '>'+ data +'</a>';
        }),
        DTColumnBuilder.newColumn('part_no').withTitle(lang.part_no).renderWith(function(data, type, full) {
          return data;
        }),
        DTColumnBuilder.newColumn('job_id').withTitle(lang.job_id),
        DTColumnBuilder.newColumn('qty').withTitle(lang.qty),
        DTColumnBuilder.newColumn('actual_date').withTitle(lang.actual_date),
        DTColumnBuilder.newColumn('del_date').withTitle(lang.delivery_date),
        DTColumnBuilder.newColumn('cust_code').withTitle(lang.cust_code),
        DTColumnBuilder.newColumn('comp_name').withTitle(lang.companyname)
    ];
    
   $scope.checkDate = function(comp_date, adj_date)
   {  var status = [];
      status[0] = "1";
      var cur_date = new Date();
      if(cur_date > new Date(comp_date)) 
        {
          status[0] = "0";
          status[1] = "delay";
          if(cur_date > new Date(adj_date))
             status[1] = "critical";
         }
        return status;
   }
   $scope.updateprocess = function(id,pdt_no)
    {
          ModalService.showModal({
            templateUrl: "ins_update_process.html",
            controller: "InsUpdateProcController",
            inputs: {
                id: id,
                pdtno : pdt_no
            }
        }).then(function(modal) {
            modal.element.modal();
            //modal.order = order;
            modal.close.then(function(result) {
              $scope.dtInstance.reloadData();
                if(result.dialogResult == 1) 
                {
                 
                }
      });
        });
  }
  updateprocess = $scope.updateprocess;

  $scope.delete_order = function(id)
  {
    if (!confirm("Are you confirm about deleting the order?")) return;
      $http.post('services/weld_delete_order.php', { order_id: id}).success(function(response) {
                  if(response.status == "success")
                  {
                      $scope.dtInstance.reloadData();
                  }
          });
  }
  delete_order = $scope.delete_order;

  $scope.getInspSocket = function()
  {
      $location.path("/inspsocket");
  }
}]);

app.controller('ErpControllerWeld', ['$scope', '$location', '$routeParams', '$http','$rootScope', 'ModalService', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', function($scope, $location, $routeParams, $http,$rootScope, ModalService,$filter, DTOptionsBuilder, DTColumnBuilder) {
  
  $scope.$parent.navSelectedItem = "weld";
  $scope.$parent.subtitle = "Weld";
  $scope.$parent.pageClass = 'page-contact';

  var lang = $scope.lang;

  $scope.dtInstance = {};

   $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_weldkiosk.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc'])
        .withOption('autoWidth', true);

        var waiting = "margin:auto; border-radius: 5px;background: #ff751a;width: 35px;height: 30px;text-align:center";

        var finished = "margin:auto; border-radius: 5px;background: #009900;width: 35px;height: 30px;text-align:center;";  

        var delay = "margin:auto; border-radius: 5px;background: #ff0000;width: 35px;height: 30px;text-align:center;"; 

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
           var rpath = full.phd;//'uploads/weldkiosk/' + full.cid  +'/' + data + '/' + full.pdt_no +'.pdf';
          var ret1 = '<a onclick=updateprocess(' + data + ',' + '"' + full.pdt_no + '"' + ')' + '><img height="32" width="32" src="images/weld_update.png"></a>';
          
          var phd_show = "enabled";
          var phd_img = "images/sheet.png";
          if(full.phd != "") 
            rpath = full.phd;
            else if(full.dd != "")
              rpath = full.dd;
              else
              {
                  phd_show = "disabled";
                  phd_img = "images/sheet_cross.png";
              }
           ret1 += '<a href=' + "/KioskClient/weld.html?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank" style=\"margin-left:5px;\"><img src="images/eye.png"></a>';
            ret1 += '<a href="' + rpath + '" target="_blank" style=\"margin-left:5px;\" class="' + phd_show + '"><img src="' + phd_img + '"></a>';
          
          return ret1;

         
          /*"/KioskClient/weld.html?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank" style=\"margin-left:5px;\"><img src="images/eye.png"></a>';
          //"/KioskClient/quote.html?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank" style=\"margin-left:5px;\"><img src="images/estimate.png"></a>';
          //'<a onclick="delete_order('+ data + ')"><img src="images/weld_delete.png"></a>';  
          
         // if(full.status == "finished")
            ret1 = ret1 + '<a href=' + rpath + ' target="_blank" style=\"margin-left:5px;\"><img src="images/sheet.png"></a>';
          
          return ret1;*/
        }),
        DTColumnBuilder.newColumn('status').withTitle(lang.status).renderWith(function(data, type, full) {
          if(data == "started") 
           return '<div style="text-align:center" ng-model=' +"status-" + full.id + '> <img height="32" width="32" src="images/weld_processing.png">  </div>';

         /* {
             var st = $scope.checkDate(full.comp_date, full.adj_date); 
             if(st[0] === "0")
                return '<div><div style="' + delay  +'"ng-model=' +"status_" + full.id + '></div></div>';
          } */
          
          else if(data == "waiting") 
              return '<div><div style="' + waiting  +'"ng-model=' +"status_" + full.id + '></div></div>';

            /*{
              var st = $scope.checkDate(full.comp_date, full.adj_date); 
              if(st[0] === "0")
                return '<div><div style="' + delay  +'"ng-model=' +"status_" + full.id + '></div></div>';
            }  */         
          else if(data === "finished") return '<div><div style="' + finished  +'"ng-model=' +"status_" + full.id + '></div></div>';
        }),
        DTColumnBuilder.newColumn('order_no').withTitle(lang.orderno),
        DTColumnBuilder.newColumn('pdt_no').withTitle(lang.pdt_no).renderWith(function(data, type, full) {
          var ret = full.id + "/" + data + "/" + full.cid;
          ret = encodeURIComponent(ret);
          return '<a href=#weld_order/' + ret + '>'+ data +'</a>';
        }),
        DTColumnBuilder.newColumn('part_no').withTitle(lang.part_no).renderWith(function(data, type, full) {
          return data;
        }),
        DTColumnBuilder.newColumn('job_id').withTitle(lang.job_id),
        DTColumnBuilder.newColumn('qty').withTitle(lang.qty),
        DTColumnBuilder.newColumn('actual_date').withTitle(lang.actual_date),
        DTColumnBuilder.newColumn('del_date').withTitle(lang.delivery_date),
        DTColumnBuilder.newColumn('cust_code').withTitle(lang.cust_code),
        DTColumnBuilder.newColumn('comp_name').withTitle(lang.companyname)
    ];
    
   $scope.checkDate = function(comp_date, adj_date)
   {  var status = [];
      status[0] = "1";
      var cur_date = new Date();
      if(cur_date > new Date(comp_date)) 
        {
          status[0] = "0";
          status[1] = "delay";
          if(cur_date > new Date(adj_date))
             status[1] = "critical";
         }
        return status;
   }
   $scope.updateprocess = function(id,pdt_no)
    {
          ModalService.showModal({
            templateUrl: "weld_update_process.html",
            controller: "WeldUpdateProcController",
            inputs: {
                id: id,
                pdtno : pdt_no
            }
        }).then(function(modal) {
            modal.element.modal();
            //modal.order = order;
            modal.close.then(function(result) {
              $scope.dtInstance.reloadData();
                if(result.dialogResult == 1) 
                {
                 
                }
      });
        });
  }
  updateprocess = $scope.updateprocess;

  $scope.delete_order = function(id)
  {
    if (!confirm("Are you confirm about deleting the order?")) return;
      $http.post('services/weld_delete_order.php', { order_id: id}).success(function(response) {
                  if(response.status == "success")
                  {
                      $scope.dtInstance.reloadData();
                  }
          });
  }
  delete_order = $scope.delete_order;
}]);
  
  app.controller('ErpControllerBend', ['$scope', '$location', '$routeParams', '$http','$rootScope', 'ModalService', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', function($scope, $location, $routeParams, $http,$rootScope, ModalService,$filter, DTOptionsBuilder, DTColumnBuilder) {
  
  $scope.$parent.navSelectedItem = "bend";
  $scope.$parent.subtitle = "Bend";
  $scope.$parent.pageClass = 'page-contact';

  var lang = $scope.lang;

  $scope.dtInstance = {};

   $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_weldkiosk.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc'])
        .withOption('autoWidth', true);

        var waiting = "margin:auto; border-radius: 5px;background: #ff751a;width: 35px;height: 30px;text-align:center";

         var started = "margin:auto; border-radius: 5px;background: #3399ff;width: 35px;height: 30px;text-align:center";

        var finished = "margin:auto; border-radius: 5px;background: #009900;width: 35px;height: 30px;text-align:center;";  

        var delay = "margin:auto; border-radius: 5px;background: #ff0000;width: 35px;height: 30px;text-align:center;"; 

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
          var rpath = 'uploads/weldkiosk/' + full.cid  +'/' + data + '/Reports/' + full.pdt_no +'-PHD.pdf';
          var ret1 = '<a onclick=updateprocess(' + data + ',' + '"' + full.pdt_no + '"' + ')' + '><img height="32" width="32" src="images/cloudicon.png"></a> <a href=' + 
          "/KioskClient/weld.html?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank" style=\"margin-left:5px;\"><img src="images/eye.png"></a>';
          //"/KioskClient/quote.html?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank" style=\"margin-left:5px;\"><img src="images/estimate.png"></a>';
          //'<a onclick="delete_order('+ data + ')"><img src="images/weld_delete.png"></a>';  
          
         // if(full.status == "finished")
            ret1 = ret1 + '<a href=' + rpath + ' target="_blank" style=\"margin-left:5px;\"><img src="images/sheet.png"></a>';
          
          return ret1;
        }),
        DTColumnBuilder.newColumn('status').withTitle(lang.status).renderWith(function(data, type, full) {
          if(data == "started") 
           return '<div><div style="' + started  +'"ng-model=' +"status_" + full.id + '></div></div>';

         /* {
             var st = $scope.checkDate(full.comp_date, full.adj_date); 
             if(st[0] === "0")
                return '<div><div style="' + delay  +'"ng-model=' +"status_" + full.id + '></div></div>';
          } */
          
          else if(data == "waiting") 
              return '<div><div style="' + waiting  +'"ng-model=' +"status_" + full.id + '></div></div>';

            /*{
              var st = $scope.checkDate(full.comp_date, full.adj_date); 
              if(st[0] === "0")
                return '<div><div style="' + delay  +'"ng-model=' +"status_" + full.id + '></div></div>';
            }  */         
          else if(data === "finished") return '<div><div style="' + finished  +'"ng-model=' +"status_" + full.id + '></div></div>';
        }),
        DTColumnBuilder.newColumn('order_no').withTitle(lang.orderno),
        DTColumnBuilder.newColumn('pdt_no').withTitle(lang.pdt_no).renderWith(function(data, type, full) {
          var ret = full.id + "/" + data + "/" + full.cid;
          ret = encodeURIComponent(ret);
          return '<a href=#bend_order/' + ret + '>'+ data +'</a>';
        }),
        DTColumnBuilder.newColumn('part_no').withTitle(lang.part_no).renderWith(function(data, type, full) {
          return data;
        }),
        DTColumnBuilder.newColumn('job_id').withTitle(lang.job_id),
        DTColumnBuilder.newColumn('qty').withTitle(lang.qty),
        DTColumnBuilder.newColumn('actual_date').withTitle(lang.actual_date),
        DTColumnBuilder.newColumn('del_date').withTitle(lang.delivery_date),
        DTColumnBuilder.newColumn('cust_code').withTitle(lang.cust_code),
        DTColumnBuilder.newColumn('comp_name').withTitle(lang.companyname)
    ];
    
   $scope.checkDate = function(comp_date, adj_date)
   {  var status = [];
      status[0] = "1";
      var cur_date = new Date();
      if(cur_date > new Date(comp_date)) 
        {
          status[0] = "0";
          status[1] = "delay";
          if(cur_date > new Date(adj_date))
             status[1] = "critical";
         }
        return status;
   }
   $scope.updateprocess = function(id,pdt_no)
    {
          ModalService.showModal({
            templateUrl: "bend_update_process.html",
            controller: "WeldUpdateProcController",
            inputs: {
                id: id,
                pdtno : pdt_no
            }
        }).then(function(modal) {
            modal.element.modal();
            //modal.order = order;
            modal.close.then(function(result) {
              $scope.dtInstance.reloadData();
                if(result.dialogResult == 1) 
                {
                 
                }
      });
        });
  }
  updateprocess = $scope.updateprocess;

  $scope.delete_order = function(id)
  {
    if (!confirm("Are you confirm about deleting the order?")) return;
      $http.post('services/weld_delete_order.php', { order_id: id}).success(function(response) {
                  if(response.status == "success")
                  {
                      $scope.dtInstance.reloadData();
                  }
          });
  }
  delete_order = $scope.delete_order;
}]);

 app.controller('AiquoteController', ['$scope', '$location', '$routeParams', '$http','$rootScope', 'ModalService', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', function($scope, $location, $routeParams, $http,$rootScope, ModalService,$filter, DTOptionsBuilder, DTColumnBuilder) {
  
  $scope.$parent.navSelectedItem = "AIQuote";
  $scope.$parent.subtitle = "AI Quote";
  $scope.$parent.pageClass = 'page-contact';

  var lang = $scope.lang;

  $scope.dtInstance = {};

   $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_aiquote.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc'])
        .withOption('autoWidth', true);

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
          var rpath = "";//'uploads/weldkiosk/' + full.cid  +'/' + data + '/Reports/' + full.pdt_no +'-PHD.pdf';
          
          var phd_show = "enabled";
          var phd_img = "images/sheet.png";
          if(full.phd != "") 
            rpath = full.phd;
            else if(full.dd != "")
              rpath = full.dd;
              else
              {
                  phd_show = "disabled";
                  phd_img = "images/sheet_cross.png";
              }
          var ret1 = '<a href=' + "/KioskApps/index.html#quote/quote3d?cid=" +full.cid+"&" + "orderid=" + data +  ' target="_blank" style=\"margin-left:5px;\"><img src="images/eye.png"></a>';
            ret1 = ret1 + '<a href="' + rpath + '" target="_blank" style=\"margin-left:5px;\" class="' + phd_show + '"><img src="' + phd_img + '"></a>';
          
          return ret1;
        }),
        DTColumnBuilder.newColumn('order_no').withTitle(lang.orderno),
        DTColumnBuilder.newColumn('pdt_no').withTitle(lang.pdt_no).renderWith(function(data, type, full) {
          var ret = full.id + "/" + data + "/" + full.cid;
          ret = encodeURIComponent(ret);
          return '<a href=#aiquote_order/' + ret + '>'+ data +'</a>';
        }),
        DTColumnBuilder.newColumn('part_no').withTitle(lang.part_no).renderWith(function(data, type, full) {
          return data;
        }),
        DTColumnBuilder.newColumn('job_id').withTitle(lang.job_id),
        DTColumnBuilder.newColumn('cust_code').withTitle(lang.cust_code),
        DTColumnBuilder.newColumn('comp_name').withTitle(lang.companyname)
    ];
    
  $scope.delete_order = function(id)
  {
    if (!confirm("Are you confirm about deleting the order?")) return;
      $http.post('services/weld_delete_order.php', { order_id: id}).success(function(response) {
                  if(response.status == "success")
                  {
                      $scope.dtInstance.reloadData();
                  }
          });
  }
  delete_order = $scope.delete_order;
}]);
app.controller('BendOrderController', ['$scope', '$location', '$routeParams', '$http','$rootScope',  function($scope, $location, $routeParams, $http,$rootScope) {

  $scope.orderid = $routeParams.id;
  var pdtno =  $routeParams.pdtno;
  $scope.dwg = "uploads/weldkiosk/10/" + $scope.orderid + "/"+ pdtno + ".pdf";
  $scope.phd = "uploads/weldkiosk/10/"  + $scope.orderid + "/" + pdtno + "-PHD.pdf";
  $scope.threeD = "/KioskClient/weld.html?cid=10" + "&" + "orderid=" + $scope.orderid;
          
}]);

app.controller('WeldOrderController', ['$scope', '$location', '$routeParams', '$http','$rootScope',  function($scope, $location, $routeParams, $http,$rootScope) {
  
  var cid = $routeParams.cid;
  $scope.orderid = $routeParams.id;
  var pdtno =  $routeParams.pdtno;
  /*$scope.dwg = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/Reports/"+ pdtno + ".pdf";
  $scope.phd = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/Reports/" + pdtno + "-PHD.pdf";
  $scope.threeD = "/KioskClient/weld.html?cid=" + cid + "&" + "orderid=" + $scope.orderid;
   */
  var path = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/";
  $scope.dwg = "";
  $scope.phd = "";
  $scope.threeD = "/kioskClient/weld.html?cid=" + cid + "&" + "orderid=" + $scope.orderid;
  $scope.csv = "";

  $scope.dwg_show = "";
  $scope.threeD_show = "";
  $scope.phd_show = "";
  $scope.csv_show = "";

  $scope.dwg_img = "dwg.png";
  $scope.threeD_img = "3d.png";
  $scope.phd_img = "phd.png";
  $scope.csv_img = "cost_attr.png";

  $http({
        method: 'POST',
        url: 'services/check_items.php',
        data : {id : $scope.orderid}
        }).then(function successCallback(response) {
           var data = response.data;

           if(data.pdf != "")
            $scope.dwg = path + data.pdf;

           else if(data.dxf != "")
           $scope.dwg = path + data.dxf;
           //else if(data.pdf != "") $scope.dwg = path + data.pdf;

           else if(data.dwg != "")
           $scope.dwg = path + data.dwg;

            else if(data.img != "")
              $scope.dwg = path + data.img;
           else 
            {
              $scope.dwg_show = "disabled";
              $scope.dwg_img = "dwg_cross.png";
            }

           if(data.phd != "") $scope.phd = path + data.phd;
           else if(data.dd != "") $scope.phd = path + data.dd;
           else 
            {
              $scope.phd_show = "disabled";
              $scope.phd_img = "phd_cross.png";
            }

           if(data.csv != "") $scope.csv = path + data.csv;
           else 
            {
              $scope.csv_show = "disabled";
              $scope.csv_img = "cost_attr_cross.png";
            }

          if(data.sat == "" & data.bmf == "" & data.assy == "") 
            {
              $scope.threeD_show = "disabled";
              $scope.threeD_img = "3d_cross.png";
              $scope.threeD = "";
            }
      });            
}]);

app.controller('AIQuoteOrderController', ['$scope', '$location', '$routeParams', '$http','$rootScope',  function($scope, $location, $routeParams, $http,$rootScope) {

  var cid = $routeParams.cid;
  $scope.orderid = $routeParams.id;
  var pdtno =  $routeParams.pdtno;

  /*$scope.dwg = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/"+ pdtno + ".dxf";
  $scope.phd = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/Reports/" + pdtno + "-PHD.pdf";
  $scope.threeD = "/KioskClient/quote.html?cid=" + cid + "&" + "orderid=" + $scope.orderid;
  $scope.csv = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/Reports/" + pdtno + ".xlsx";
  */
  var path = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/";
  $scope.dwg = "";
  $scope.phd = "";
  $scope.threeD = "/KioskApps/index.html#quote/quote3d?cid=" + cid + "&" + "orderid=" + $scope.orderid;
  $scope.csv = "";

  $scope.dwg_show = "";
  $scope.threeD_show = "";
  $scope.phd_show = "";
  $scope.csv_show = "";

  $scope.dwg_img = "dwg.png";
  $scope.threeD_img = "3d.png";
  $scope.phd_img = "phd.png";
  $scope.csv_img = "cost_attr.png";

  $http({
        method: 'POST',
        url: 'services/check_items.php',
        data : {id : $scope.orderid}
        }).then(function successCallback(response) {
           var data = response.data;

           if(data.pdf != "")
            $scope.dwg = path + data.pdf;
           else if(data.dxf != "")
           $scope.dwg = path + data.dxf;
           //else if(data.pdf != "") $scope.dwg = path + data.pdf;

           else if(data.dwg != "")
           $scope.dwg = path + data.dwg;

            else if(data.img != "")
              $scope.dwg = path + data.img;
           else 
            {
              $scope.dwg_show = "disabled";
              $scope.dwg_img = "dwg_cross.png";
            }

           if(data.phd != "") $scope.phd = path + data.phd;
           else if(data.dd != "") $scope.phd = path + data.dd;
           else 
            {
              $scope.phd_show = "disabled";
              $scope.phd_img = "phd_cross.png";
            }

           if(data.csv != "") $scope.csv = path + data.csv;
           else 
            {
              $scope.csv_show = "disabled";
              $scope.csv_img = "cost_attr_cross.png";
            }

          if(data.sat == "" & data.bmf == "" & data.assy == "") 
            {
              $scope.threeD_show = "disabled";
              $scope.threeD_img = "3d_cross.png";
              $scope.threeD = "";
            }
      });        
}]);

app.controller('InspOrderController', ['$scope', '$location', '$routeParams', '$http','$rootScope',  function($scope, $location, $routeParams, $http,$rootScope) {

  var cid = $routeParams.cid;
  $scope.orderid = $routeParams.id;
  var pdtno =  $routeParams.pdtno;

  var path = "uploads/weldkiosk/" + cid + "/" + $scope.orderid + "/";
  $scope.dwg = "";
  $scope.phd = "";
  $scope.threeD = "/kioskinspection/index.html?cid=" + cid + "&" + "orderid=" + $scope.orderid;
  $scope.csv = "";

  $scope.dwg_show = "";
  $scope.threeD_show = "";
  $scope.phd_show = "";
  $scope.csv_show = "";

  $scope.dwg_img = "dwg.png";
  $scope.threeD_img = "3d.png";
  $scope.phd_img = "phd.png";
  $scope.csv_img = "cost_attr.png";

  $http({
        method: 'POST',
        url: 'services/check_items.php',
        data : {id : $scope.orderid}
        }).then(function successCallback(response) {
           var data = response.data;
           
           if(data.dxf == "")
           {
            $scope.threeD_show = "disabled";
              $scope.threeD_img = "3d_cross.png";
           }
           
           if(data.pdf != "")
            $scope.dwg = path + data.pdf;

           else if(data.dxf != "")
           $scope.dwg = path + data.dxf;
           //else if(data.pdf != "") $scope.dwg = path + data.pdf;

           else if(data.dwg != "")
           $scope.dwg = path + data.dwg;

            else if(data.img != "")
              $scope.dwg = path + data.img;
           else 
            {
              $scope.dwg_show = "disabled";
              $scope.dwg_img = "dwg_cross.png";
            }

           if(data.phd != "") $scope.phd = path + data.phd;
           else if(data.dd != "") $scope.phd = path + data.dd;
           else 
            {
              $scope.phd_show = "disabled";
              $scope.phd_img = "phd_cross.png";
            }

           if(data.csv != "") $scope.csv = path + data.csv;
           else 
            {
              $scope.csv_show = "disabled";
              $scope.csv_img = "cost_attr_cross.png";
            }
      });
}]);

app.controller('UpdateProcController', ['$scope', '$element','$filter', 'close', 
  function($scope, $element,$filter, close) {
  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
 	  close({
	  dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
}]);

app.controller('WeldUpdateProcController', ['$scope', '$rootScope','$element','$filter', '$http', 'close', 'id', 'pdtno', '$location',
  function($scope, $rootScope, $element,$filter,$http, close, id, pdtno, $location) {

  var lang = $scope.lang;
  $http.post('services/weld_get_time.php',{order_id: id}).success(function(response) {
        
        if(response.result === "failed")
        {
          var date = new Date();
          $scope.start_time = $filter('date')(date,'yyyy-MM-dd  HH:mm:ss');
          $scope.update_start_time();
        }
        else
        {
          $scope.start_time = response.start_time;
          $scope.end_time = response.end_time;
          $scope.oper_name = response.oper_name;
          $scope.weld_mach = response.weld_mach;
          $scope.comments = response.comments;
        }
    }); 

  $scope.update_start_time = function()
  {
    $http.post('services/weld_update_time.php', { order_id: id, start_time: $scope.start_time }).success(function(response) {
                  if(response.status == "success" & response.status_update == "success")
                  {
                  }
          });
  }

  $scope.play = function()
  {
    var date = new Date();
    $scope.start_time = $filter('date')(date,'yyyy-MM-dd  HH:mm:ss');
    $scope.update_start_time();
  }
  $scope.stop = function()
  {
    var date = new Date();
    $scope.end_time = $filter('date')(date,'yyyy-MM-dd  HH:mm:ss');
  }
  $scope.reset = function()
  {
    $scope.start_time = "";
    $scope.end_time = "";

    $http.post('services/weld_update_reset.php', { order_id: id}).success(function(response) {
                  if(response.status == "success")
                  {
                  }
          });
  }
  $scope.submit = function()
  {
    function check_value(data)
    {
        if(data == "" || data == null) return true;
        else return false;
    }

    if(check_value($scope.start_time) || check_value($scope.end_time)) 
      {
        alert("Please provide process start/end time"); 
        return;
      }
    $http.post('services/weld_update_submit.php', 
      { order_id: id, 
        start_time : $scope.start_time, 
        end_time   : $scope.end_time,
        oper_name  : $scope.oper_name,
        weld_mach  : $scope.weld_mach,
        comments   : $scope.comments,
       }).success(function(response) {
                  if(response.status == "success")
                  {
                    $scope.close();
                    var path = "/weld_photo/" + $scope.start_time + "/" + $scope.end_time + "/" + id + "/" + response.pid + "/" + pdtno;
                    $location.path(path);
                  }
          });
  }

  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1,
    status : $scope.status
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0,
      status : $scope.status
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
}]);

app.controller('InsUpdateProcController', ['$scope', '$rootScope','$element','$filter', '$http', 'close', 'id', 'pdtno', '$location',
  function($scope, $rootScope, $element,$filter,$http, close, id, pdtno, $location) {
  
  var lang = $scope.lang;
  $http.post('services/ins_get_time.php',{order_id: id}).success(function(response) {
        
        if(response.result === "failed")
        {
          var date = new Date();
          $scope.start_time = $filter('date')(date,'yyyy-MM-dd  HH:mm:ss');
          $scope.update_start_time();
        }
        else
        {
          $scope.start_time = response.start_time;
          $scope.end_time = response.end_time;
          $scope.oper_name = response.oper_name;
          //$scope.weld_mach = response.weld_mach;
          $scope.comments = response.comments;
        }
    }); 

  $scope.update_start_time = function()
  {
    $http.post('services/ins_update_time.php', { order_id: id, start_time: $scope.start_time }).success(function(response) {
                  if(response.status == "success" & response.status_update == "success")
                  {
                  }
          });
  }

  $scope.play = function()
  {
    var date = new Date();
    $scope.start_time = $filter('date')(date,'yyyy-MM-dd  HH:mm:ss');
    $scope.update_start_time();
  }
  $scope.stop = function()
  {
    var date = new Date();
    $scope.end_time = $filter('date')(date,'yyyy-MM-dd  HH:mm:ss');
  }
  $scope.reset = function()
  {
    $scope.start_time = "";
    $scope.end_time = "";

    $http.post('services/ins_update_reset.php', { order_id: id}).success(function(response) {
                  if(response.status == "success")
                  {
                  }
          });
  }
  $scope.submit = function()
  {
    function check_value(data)
    {
        if(data == "" || data == null) return true;
        else return false;
    }

    if(check_value($scope.start_time) || check_value($scope.end_time)) 
      {
        alert("Please provide process start/end time"); 
        return;
      }
    $http.post('services/ins_update_submit.php', 
      { order_id: id, 
        start_time : $scope.start_time, 
        end_time   : $scope.end_time,
        oper_name  : $scope.oper_name,
        //weld_mach  : $scope.weld_mach,
        comments   : $scope.comments,
       }).success(function(response) {
                  if(response.status == "success")
                  {
                    $scope.close();
                    var path = "/ins_photo/" + $scope.start_time + "/" + $scope.end_time + "/" + id + "/" + response.pid + "/" + pdtno;
                    $location.path(path);
                  }
          });
  }

  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1,
    status : $scope.status
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0,
      status : $scope.status
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
}]);
app.controller('WeldPhotoController', ['$scope', '$rootScope', '$http', '$location', '$routeParams', '$document', function($scope, $rootScope, $http, $location, $routeParams,$document) {
  
  $scope.$parent.navSelectedItem = "WeldPhoto";
  $scope.$parent.subtitle = "Weld Photo";
  $scope.$parent.pageClass = 'page-contact';

  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        $scope.customer = response.data;
      });

  var lang = $scope.lang;
  $scope.imgClass = "col-xs-6 col-sm-4 col-md-3 col-lg-3";
   $scope.imgHeight = "232";
   $scope.imgWidth = "232";

  $scope.start_time = $routeParams.stime;
  $scope.end_time = $routeParams.etime;
  $scope.order_id = $routeParams.oid;
  $scope.p_id = $routeParams.pid;
  $scope.pdtno = $routeParams.pdtno;
  var date1 = $scope.start_time.split(" ");
  var date2 = $scope.end_time.split(" ");
  var sdate = date1["0"];
  var stime = date1["1"];
  var edate = date2["0"];
  var etime = date2["1"];

  $http({
    method: 'POST',
    url: 'services/get_weld_photo.php',
    data : {sdate : sdate, edate : edate, stime : stime, etime: etime},
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function successCallback(response) {
        $scope.images = response.data;
        if($scope.images.length == 0) 
          {
            $("#heading").text("");
            $("#show_empty").text("No photos to show, Please click submit");
          }
    });

    $scope.single_click = function(obj)
   {
    var item = obj.currentTarget;

    var res =  $document.find('#'+item.id); 
    var bool = item.getAttribute("selected");
    //item.remove();
    if(bool === "true")
    {
      res.css("border", "1px ridge #ffffff");
      item.setAttribute("selected","false");
    }
    else
    {
       res.css( "border", "3px ridge #ff6600" );
       item.setAttribute("selected",true);
    }
   }

   $scope.submit = function()
   {
    if($scope.customer.id == "10") 
      {
        $location.path("/bend");
        return;
      }
      var files = [];
      var ids = [];
        var res = $document.find('[selected="true"]');
        for (var i = 0; i < res.length; i++)
        {
            //console.log(res[i].attributes["src"].nodeValue);
            var path = res[i].attributes["src"].nodeValue;
            var id = res[i].attributes["id"].nodeValue;
            var filename = path.replace(/^.*[\\\/]/, '');
            //filename = filename.substring(1);
            var ind1 = path.lastIndexOf("/");
            var fn1 = path.substring(0,ind1);
            var ind2 = fn1.lastIndexOf("/");
            var fn2 = fn1.substring(ind2+1);
            filename = fn2 + "/" + filename;
            
            files.push(filename);
            ids.push(id);
        }
        /*if(files.length < 1) 
          {
            alert("Please select photos");
            return;
          }*/
          //$location.path("/nishiyama/weld");
          var request = $http({
                        method: "post",
                        url: 'services/weld_photo_submit.php',
                        data: { files : files, ids : ids, order_id : $scope.order_id, p_id : $scope.p_id, pdt_no : $scope.pdtno},
                        responseType: 'arraybuffer'
            });
            request.success(function(data) {
              
               $location.path("/weld");
            });
   }

  }]);


app.controller('InsPhotoController', ['$scope', '$rootScope', '$http', '$location', '$routeParams', '$document', function($scope, $rootScope, $http, $location, $routeParams,$document) {
  
  $scope.$parent.navSelectedItem = "InspectionPhoto";
  $scope.$parent.subtitle = "Inspection Photo";
  $scope.$parent.pageClass = 'page-contact';

  var lang = $scope.lang;
  $scope.imgClass = "col-xs-6 col-sm-4 col-md-3 col-lg-3";
   $scope.imgHeight = "232";
   $scope.imgWidth = "232";

  $scope.start_time = $routeParams.stime;
  $scope.end_time = $routeParams.etime;
  $scope.order_id = $routeParams.oid;
  $scope.p_id = $routeParams.pid;
  $scope.pdtno = $routeParams.pdtno;
  var date1 = $scope.start_time.split(" ");
  var date2 = $scope.end_time.split(" ");
  var sdate = date1["0"];
  var stime = date1["1"];
  var edate = date2["0"];
  var etime = date2["1"];

  $http({
    method: 'POST',
    url: 'services/get_weld_photo.php',
    data : {sdate : sdate, edate : edate, stime : stime, etime: etime},
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function successCallback(response) {
        $scope.images = response.data;
        if($scope.images.length == 0) 
          {
            $("#heading").text("");
            $("#show_empty").text("No photos to show, Please click submit");
          }
    });

    $scope.single_click = function(obj)
   {
    var item = obj.currentTarget;

    var res =  $document.find('#'+item.id); 
    var bool = item.getAttribute("selected");
    //item.remove();
    if(bool === "true")
    {
      res.css("border", "1px ridge #ffffff");
      item.setAttribute("selected","false");
    }
    else
    {
       res.css( "border", "3px ridge #ff6600" );
       item.setAttribute("selected",true);
    }
   }

   $scope.submit = function()
   {
      var files = [];
      var ids = [];
        var res = $document.find('[selected="true"]');
        for (var i = 0; i < res.length; i++)
        {
            //console.log(res[i].attributes["src"].nodeValue);
            var path = res[i].attributes["src"].nodeValue;
            var id = res[i].attributes["id"].nodeValue;
            var filename = path.replace(/^.*[\\\/]/, '');
            //filename = filename.substring(1);
            var ind1 = path.lastIndexOf("/");
            var fn1 = path.substring(0,ind1);
            var ind2 = fn1.lastIndexOf("/");
            var fn2 = fn1.substring(ind2+1);
            filename = fn2 + "/" + filename;
            
            files.push(filename);
            ids.push(id);
        }
        /*if(files.length < 1) 
          {
            alert("Please select photos");
            return;
          }*/
          //$location.path("/nishiyama/weld");
          var request = $http({
                        method: "post",
                        url: 'services/ins_photo_submit.php',
                        data: { files : files, ids : ids, order_id : $scope.order_id, p_id : $scope.p_id, pdt_no : $scope.pdtno},
                        responseType: 'arraybuffer'
            });
            request.success(function(data) {
                $location.path("/insroom");
            });
   }

  }]);

app.directive('dropzoneOffice', function() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {

            var uploadURL = 'services/upload_office.php?user=' + global.username;
            var config = {
                url: uploadURL,
                maxFilesize: 5000,
                paramName: "file",
                maxThumbnailFilesize: 10,
                parallelUploads: 100,
                autoProcessQueue: true,
                addRemoveLinks: true,
                removedfile: function(file) {
                        var filename = file.name;  
                    
                        // Todo : call ajax post to call remove_file.php and pass filename
                $.http({
                         method : 'POST',
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},           
                  data : {filename: filename},

                
            function(data, status){
              if(!status){
                //alert("please try again");
              }                       
            }});
            
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;       
                    }
            };

            var eventHandlers = {
                'addedfile': function(file) {
                    scope.file = file;
                    if (this.files[1]!=null) {
                        //this.removeFile(this.files[0]);
                    }
                    scope.$apply(function() {
                        scope.fileAdded = true;
                    });
                    
                },

                'success': function (file, response) {
                }

            };

            dropzoneOffice = new Dropzone(element[0], config);

            angular.forEach(eventHandlers, function(handler, event) {
                dropzoneOffice.on(event, handler);
            });

            scope.processDropzone = function() {
                dropzoneOffice.processQueue();
            };

            scope.resetDropzone = function() {
                dropzoneOffice.removeAllFiles();
            }
        }
    }
});

app.directive('dropzoneDrawManager', function() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {

            var uploadURL = 'services/upload_draw_manager.php';
            var config = {
                url: uploadURL,
                maxFilesize: 5000,
                paramName: "file",
                maxThumbnailFilesize: 10,
                parallelUploads: 100,
                autoProcessQueue: true,
                addRemoveLinks: true,
                removedfile: function(file) {
                        var filename = file.name;  
                    
                        // Todo : call ajax post to call remove_file.php and pass filename
                $.http({
                         method : 'POST',
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},           
                  data : {filename: filename},

                
            function(data, status){
              if(!status){
                  //alert("please try again");
              }                       
            }});
            
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;       
                    }
            };

            var eventHandlers = {
                'addedfile': function(file) {
                    scope.file = file;
                    if (this.files[1]!=null) {
                        //this.removeFile(this.files[0]);
                    }
                    scope.$apply(function() {
                        scope.fileAdded = true;
                    });
                    
                },

                'success': function (file, response) {
                  //alert("please try again");
                }

            };

            dropzoneDrawManager = new Dropzone(element[0], config);

            angular.forEach(eventHandlers, function(handler, event) {
                dropzoneDrawManager.on(event, handler);
            });

            scope.processDropzone = function() {
                dropzoneDrawManager.processQueue();
            };

            scope.resetDropzone = function() {
                dropzoneDrawManager.removeAllFiles();
            }
        }
    }
});

