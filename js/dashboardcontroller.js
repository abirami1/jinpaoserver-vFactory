var app = angular.module('ProjectTrackingModule');

app.controller('DashboardController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
	
  $scope.$parent.navSelectedItem = "home";
  $scope.$parent.subtitle = $scope.lang.dashboard;
  
  $scope.pageClass = 'page-contact';
  
  $http.get("services/order_summary.php")
    .success(function (response) {
		
	   $scope.orders = response;
     $scope.piedata = [{
                          value: response.phd2d4d,
                          color: "#1ac6ff",
                          label: "2Dto4D"}, 
                          {
                          value: response.phd3d4d,
                          color: "#66cc00",
                          label: "3Dto4D"}, 
                          {
                          value: response.phd2d3d,
                          color: "#ff8c1a",
                          label: "2Dto3D"
                      }];

      var pieoptions = {
      segmentShowStroke: false,
      animateRotate: true,
      animateScale: true
      }

      var ctxpie = document.getElementById("pie").getContext("2d");
      var pieChart = new Chart(ctxpie).Pie($scope.piedata,pieoptions);
    
      var ipfiles = response.ipfiles;
      
      $scope.piedata1 = [{
                            value: ipfiles.v2dto3d.Waiting,
                            color: "#ff4d4d",
                            label: "Waiting"}, 
                            {
                            value: ipfiles.v2dto3d.Processing,
                            color: "#ff751a",
                            label: "Processing"}, 
                            {
                            value: ipfiles.v2dto3d.Finished,
                            color: "#33cc33",
                            label: "Finished"
                        }];

      var ctxpie1 = document.getElementById("pie1").getContext("2d");
      $scope.pieChart1 = new Chart(ctxpie1).Pie($scope.piedata1,pieoptions);
      $scope.showPie1 = ($scope.pieChart1.total) > 0 ? "true" : "false";

       $scope.piedata2 = [{
                            value: ipfiles.v3dto4d.Waiting,
                            color: "#ff4d4d",
                            label: "Waiting"}, 
                            {
                            value: ipfiles.v3dto4d.Processing,
                            color: "#ff751a",
                            label: "Processing"}, 
                            {
                            value: ipfiles.v3dto4d.Finished,
                            color: "#33cc33",
                            label: "Finished"
                        }];

      var ctxpie2 = document.getElementById("pie2").getContext("2d");
      $scope.pieChart2 = new Chart(ctxpie2).Pie($scope.piedata2,pieoptions);
      $scope.showPie2 = ($scope.pieChart2.total > 0) ? "true" : "false";

       $scope.piedata3 = [{
                           value: ipfiles.v2dto4d.Waiting,
                            color: "#ff4d4d",
                            label: "Waiting"}, 
                            {
                            value: ipfiles.v2dto4d.Processing,
                            color: "#ff751a",
                            label: "Processing"}, 
                            {
                            value: ipfiles.v2dto4d.Finished,
                            color: "#33cc33",
                            label: "Finished"
                        }];

      var ctxpie3 = document.getElementById("pie3").getContext("2d");
      $scope.pieChart3 = new Chart(ctxpie3).Pie($scope.piedata3,pieoptions);
      $scope.showPie3 = ($scope.pieChart3.total) > 0 ? "true" : "false";

      $scope.bardata =  {
      labels: response.fupdates,
      datasets: [
          {
              label: "My First dataset",
              fillColor: "#b266ff",
              strokeColor: "rgba(220,220,220,0.8)",
              highlightFill: "#9933ff",
              highlightStroke: "#7f00ff",
              data: response.fupcounts
          }]
        };

      var baroptions = {
        scaleBeginAtZero : true,
        scaleGridLineWidth : 1,
        scaleFontFamily: "'Helvetica Neue',sans-serif",
        scaleFontSize: 13,
        scaleFontColor: "#1a1a1a",
      }
      var ctxbar = document.getElementById("bar").getContext("2d");
      var barChart = new Chart(ctxbar).Bar($scope.bardata,baroptions);
  });

	$scope.showComplex = function() {

    ModalService.showModal({
      templateUrl: "complex.html",
      controller: "CustomController",
      inputs: {
        title: "A More Complex Example"
      }
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
        $scope.complexResult  = "Name: " + result.name + ", age: " + result.age;
      });
    });

  };
}]);