var app = angular.module('ProjectTrackingModule');

app.controller('AddFilesController', ['$scope', '$element', 'close', 
  function($scope, $element, close) {

  //alert(global.currentOrder.id);
  //alert(global.currentOrder.customerid);
  
  $scope.orderid = global.currentOrder.id;

  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
 	  close({
	  dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };


}]);

