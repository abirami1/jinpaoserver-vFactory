var app = angular.module('ProjectTrackingModule');

app.controller('AddStatusController', ['$scope', '$element', 'close', 
  function($scope, $element, close) {

  $scope.status = null;
  $scope.note = null;

  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
    
    if($scope.status == null || $scope.status == "")
    {
      alert("Enter the subject");
      return;
    }
    if($scope.note == null || $scope.note == "")
    {
      alert("Enter the message");
      return;
    }
    
    //  Manually hide the modal.
    $element.modal('hide');
    
 	  close({
      status: $scope.status,
      note: $scope.note,
	  dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      status: $scope.status,
      note: $scope.note,
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };


}]);

