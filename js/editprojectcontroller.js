
var app = angular.module('ProjectTrackingModule');

app.controller('EditProjectController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {

    $scope.$parent.navSelectedItem = "orders";
    $scope.pageClass = 'page-contact';
    
    window.scrollTo(0, 0);  

    var editProject = this;
    
    var projectId = $routeParams.id,
        projectIndex;
        
    // initialize fields
    $scope.itemsOrderType = [ {id: 1, label: '2D to Q3D'},{id: 2, label: '2D to 3D'}, {id: 3, label: '3D to 4D'}, {id: 4, label: '2D to 4D'}];
    
    function getOrder() 
    {
        var request = $http({
        method: "post",
        url: 'services/get_order.php',
        data: { orderid : projectId },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    });
    request.success(function(response) {
            // success
            $scope.order = response;
            $scope.orderType = $scope.itemsOrderType[$scope.order.service-1];
            
    });    
    } 

    getOrder();   
    $scope.parseDate = function(dtStr)
    {
        if(dtStr === undefined) return new Date();
        
        var dateTime = dtStr.split(/[\s-:]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2], dateTime[3], dateTime[4], dateTime[5]); // months are indexed in js
    }
     
    $scope.save = function(newdata) 
    {        
        var request = $http({
        method: "post",
        url: 'services/update_order.php',
        data:  $scope.order,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        request.success(function(response) {
            // success
           alert(response);
            
        }); 
    };
    $scope.add_status = function()
    {
      alert("status");
      $scope.add_status_model = true;
      //$("#add_status_model").css({'opacity':'1', 'display':'block'});  
      //$("#add_status_model").modal('show');
     
        
    };
    $scope.showModal = false;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    
    $scope.showComplex = function() {

        ModalService.showModal({
            templateUrl: "add_status.html",
            controller: "AddStatusController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                
                result.orderid = projectId;
                
                var request = $http({
                    method: "post",
                    url: 'services/update_status.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {
                       // $scope.order.status = response;
                        getOrder();
                        // success
                        /*$scope.order.forEach(function($row){
                            var cdate = new Date();
                            var c = cdate.getFullYear() +"-" + (cdate.getMonth()+1) + "-" + cdate.getDate() + " " + cdate.getHours() + ":" + cdate.getMinutes() +":" + cdate.getSeconds();
                            var status1 = {"status":result.status, "note":result.note, "date":c}; 
                            $row.status.push(status1);    
                        });*/
                });
            });
        });

    };
	
	$scope.add_file = function(order, role)
	{
		global.currentOrder = order;
        global.currentOrder.role = role;
        
        ModalService.showModal({
            templateUrl: "add_files.html",
            controller: "AddFilesController",
        }).then(function(modal) {
            modal.element.modal();
            //modal.order = order;
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                
                result.orderid = order.id;
                result.customerid = order.customerid;

                var request = $http({
                    method: "post",
                    url: 'services/update_filestatus.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {
                        getOrder(); 
                });
            });
        });
	};
    
    
 }]);
  
