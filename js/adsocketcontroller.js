var app = angular.module('ProjectTrackingModule');

var addToOrder, deleteItem,editScan,editRouter;

app.controller('AdSocketController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "aD Socket";
  $scope.$parent.pageClass = 'page-about';
}]);

app.controller('ErpController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "ERP Socket";
  $scope.$parent.pageClass = 'page-contact';  
  
}]);

app.controller('MOSocketController', ['$scope', '$http', '$filter', 'ModalService', function($scope, $http, $filter, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "MO Socket";
  $scope.$parent.pageClass = 'page-contact';  
  $scope.dtInstance = {};
  
  $scope.placeOrder = function(f)
  {
  
    ModalService.showModal({
            templateUrl: "place_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                //result['filename'] = f.filename;
                //result['socket_type'] = f.stype;
				result['moId'] = f.id;
				result['status'] = f.status;
				
                var request = $http({
                    method: "post",
                    url: 'services/submit_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
        });
    });
  }
  
  placeOrder = $scope.placeOrder;
  
  $scope.deleteRow = function(id) 
    { 
        if (confirm("Do you want to delete the order - " + id + " ?")) {
    
		var request = $http({
			method: "post",
			url: 'services',
			data: { orderid : id },
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		});
		request.success(function(response) {            
            var resetPaging = false;
            $scope.dtInstance.reloadData(callback, resetPaging);
            
            function callback(json) {
               console.log(json);
            }			
		  }); 
        }
	};
    
    deleteRow = $scope.deleteRow;
	
	$http.get("services/get_erp_data.php")
    .success(function (response) {
    
      $scope.modata = response.data;
  });

}
]);

app.controller('ErpDataController', ['$scope', '$http', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', 'ModalService', function($scope, $http, $filter, 
	DTOptionsBuilder, DTColumnBuilder, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "ERP Socket";
  $scope.$parent.pageClass = 'page-contact';  
  $scope.dtInstance = {};
  
  window.scrollTo(0, 0);  
  
  // $scope.dtOptions = DTOptionsBuilder.newOptions()
        // .withOption('ajax', {         
         // url: 'services/get_erp_data.php',
         // type: 'POST'
     // })
     // .withDataProp('data')
        // .withOption('processing', true)
        // .withOption('serverSide', true)
        // .withPaginationType('full_numbers')
        // .withOption('order', [0, 'desc']);
		
    // $scope.dtColumns = [
        // DTColumnBuilder.newColumn('mo_number').withTitle('MO #'),
        // DTColumnBuilder.newColumn('plan_start_date').withTitle('Plan Start Date').renderWith(function(data, type, full) {
            // return $filter('date')(data, 'yyyy/M/dd');
        // }),
		 // DTColumnBuilder.newColumn('plan_end_date').withTitle('Plan End Date').renderWith(function(data, type, full) {
            // return $filter('date')(data, 'yyyy/M/dd');
        // }),      
        // DTColumnBuilder.newColumn('status').withTitle('Status'),
        // DTColumnBuilder.newColumn('id').withTitle('Action').renderWith(function(data, type, full) {
            // return '<a onclick="placeOrder(' + data + ')"><img src="images/pencil-icon.png"/></a>' + 
           // '<a onclick="deleteRow(' + data + ')"><img src="images/minus-icon.png"></a>';
        // }),
    // ];
    
    $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
}

]);

var editInsp;
app.controller('InspSocketController', ['$scope', '$location', '$routeParams', '$filter','$http', 'DTOptionsBuilder', 'DTColumnBuilder','ModalService', function($scope, $location, $routeParams, $filter, $http, DTOptionsBuilder, DTColumnBuilder, ModalService) {

  var lang = $scope.lang;

  $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }

  var lang = $scope.lang;

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_inspsocket.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle("Drawing No").renderWith(function(data,type,full)
   {
      return data;
   }),
     
   DTColumnBuilder.newColumn('filename').withTitle("Drawing Name").renderWith(function(data,type,full)
    {
     return '<a style="margin-left:10px" href= "' + full.href + '" target="_blank" >' + data + '</a><br>';
     
    }),
   DTColumnBuilder.newColumn('cdate').withTitle(lang.date).renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('id').withTitle("Customer").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Material").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Delivery Date").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Quantity").renderWith(function(data,type,full)
    {
      return "";  
    }),
   DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
    var filename = encodeURIComponent(full.filename);
      return '<button class= "imgbtn"' +  'onclick=addToOrder("' + filename + '","' + full.stype + '")' + ' style="padding-left:5px"><img src="images/add.png"></button>' +
            '<button class="imgbtn" onclick=deleteItem("' + filename + '","' + full.stype + '")' +  ' style="padding-left:5px"><img src="images/delete.png"></button>' +
            '<button class="imgbtn" onclick=editInsp("' + full.id + '","' + filename + '")' +  ' style="padding-left:5px"><img src="images/pencil-icon.png"></button>';
   })
   ];

   /*$http.get("services/getscan.php")
    .success(function (response) {
    
      $scope.scanfiles = response;
  });*/
  
  $scope.addToOrder = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = filename;
                result['socket_type'] = stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
            }); 
          }
  }

  $scope.editInsp = function(id,filename)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "edit_insp.html",
            controller: "EditInspController",
            inputs: {
                filename: filename,
                id : id
            }
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }

                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
                  
            });
        });
  }
  addToOrder = $scope.addToOrder;
  deleteItem = $scope.deleteitem;
  editInsp =  $scope.editInsp;

}]);

app.controller('Ap100Controller', ['$scope', '$location', '$routeParams', '$filter','$http', 'DTOptionsBuilder', 'DTColumnBuilder','ModalService', function($scope, $location, $routeParams, $filter, $http, DTOptionsBuilder, DTColumnBuilder, ModalService) {

  var lang = $scope.lang;

  $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }

  var lang = $scope.lang;

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_ap100socket.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle("No").renderWith(function(data,type,full)
   {
      return data;
   }),
     
   DTColumnBuilder.newColumn('filename').withTitle("BMF Name").renderWith(function(data,type,full)
    {
     return '<a style="margin-left:10px" href= "' + full.href + '" target="_blank" >' + data + '</a><br>';
     
    }),
   DTColumnBuilder.newColumn('cdate').withTitle(lang.date).renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('no_bends').withTitle("No.Bends").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Material").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Thickness").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("No.Holes").renderWith(function(data,type,full)
    {
      return "";  
    }),
   DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
    var filename = encodeURIComponent(full.filename);

    var quoteUrl = "http://alfadock-pro.com/KioskApps/index.html#quote/quote3d?cid=" + full.custid + "&orderid="+ filename;

      return '<button class= "imgbtn"' +  'onclick=addToOrder("' + filename + '","' + full.stype + '")' + ' style="padding-left:5px"><img src="images/add.png"></button>' +
            '<button class="imgbtn" onclick=deleteItem("' + filename + '","' + full.stype + '")' +  ' style="padding-left:5px"><img src="images/delete.png"></button>' +
            '<a style="margin-left:10px" href= "' + quoteUrl + '" target="_blank" >' + '<button class="imgbtn" style="padding-left:5px"><img src="images/aiquote_.png"></button>' + '</a>'  ;
           
   })
   ];

   /*$http.get("services/getscan.php")
    .success(function (response) {
    
      $scope.scanfiles = response;
  });*/
  
  $scope.addToOrder = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = filename;
                result['socket_type'] = stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
            }); 
          }
  }

  
  addToOrder = $scope.addToOrder;
  deleteItem = $scope.deleteitem;

}]);
app.controller('Cad2dController', ['$scope', '$location', '$routeParams', '$http', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder','ModalService', function($scope, $location, $routeParams, $http,  $filter, DTOptionsBuilder, DTColumnBuilder, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "2D CAD Socket";
  $scope.$parent.pageClass = 'page-contact';

 $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }

  var lang = $scope.lang;

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get2dcad.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle("S.No").renderWith(function(data,type,full)
   {
      return data;
   }),
     
   DTColumnBuilder.newColumn('filename').withTitle(lang.filename).renderWith(function(data,type,full)
    {
     return '<a style="margin-left:10px" href= "' + full.href + '" target="_blank" >' + data + '</a><br>';
     
    }),
    DTColumnBuilder.newColumn('filename').withTitle("Type").renderWith(function(data,type,full)
   {
      return data.split(".").pop();

   }),
   
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('cdate').withTitle("Creation Date").renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('mdate').withTitle("Modified Date").renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
    var filename = encodeURIComponent(full.filename);
      return '<button class= "imgbtn"' +  'onclick=addToOrder("' + filename + '","' + full.stype + '")' + ' style="padding-left:5px"><img src="images/add.png"></button>' +
            '<button class="imgbtn" onclick=deleteItem("' + filename + '","' + full.stype + '")' +  ' style="padding-left:5px"><img src="images/delete.png"></button>' ;
   })
   ];

    $scope.addToOrder = function(f)
  {
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = f.filename;
                result['socket_type'] = f.stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }
  $scope.deleteitem = function(filename,stype)
  {
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var index = -1;    
                var filesArr = eval( $scope.files2d );
                for( var i = 0; i < filesArr.length; i++ ) 
                  {
                    if( filesArr[i].stype === stype & filesArr[i].filename === filename) 
                    {
                    index = i;
                    break;
                    }
                  }
                  if( index === -1 ) {
                   alert( "Something gone wrong" );
                  }
                  $scope.files2d.splice( index, 1 ); 
            }); 
      }
  }

  addToOrder = $scope.addToOrder;
  deleteItem = $scope.deleteitem;

}]);

app.controller('CadController', ['$scope', '$location', '$routeParams', '$http', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', 'ModalService', function($scope, $location, $routeParams, $http, $filter, DTOptionsBuilder, DTColumnBuilder, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "3D CAD Socket";
  $scope.$parent.pageClass = 'page-contact';

   $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }

  var lang = $scope.lang;

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get3dcad.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle("S.No").renderWith(function(data,type,full)
   {
      return data;
   }),
     
   DTColumnBuilder.newColumn('filename').withTitle(lang.filename).renderWith(function(data,type,full)
    {
     return '<a style="margin-left:10px" href= "' + full.href + '" target="_blank" >' + data + '</a><br>';
     
    }),
    DTColumnBuilder.newColumn('filename').withTitle("Type").renderWith(function(data,type,full)
   {
      return data.split(".").pop();
   }),
   
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('cdate').withTitle("Creation Date").renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('mdate').withTitle("Modified Date").renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
    var filename = encodeURIComponent(full.filename);
      return '<button class= "imgbtn"' +  'onclick=addToOrder("' + filename + '","' + full.stype + '")' + ' style="padding-left:5px"><img src="images/add.png"></button>' +
            '<button class="imgbtn" onclick=deleteItem("' + filename + '","' + full.stype + '")' +  ' style="padding-left:5px"><img src="images/delete.png"></button>' ;
   })
   ];
  
  $scope.addToOrder = function(f)
  {
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = f.filename;
                result['socket_type'] = f.stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var index = -1;    
                var filesArr = eval( $scope.files3d );
                for( var i = 0; i < filesArr.length; i++ ) 
                  {
                    if( filesArr[i].stype === stype & filesArr[i].filename === filename) 
                    {
                    index = i;
                    break;
                    }
                  }
                  if( index === -1 ) {
                   alert( "Something gone wrong" );
                  }
                  $scope.files3d.splice( index, 1 ); 
            }); 
          }
  }

  addToOrder = $scope.addToOrder;
  deleteItem = $scope.deleteitem;
}]);

app.controller('Ap100SetupController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "AP100 Setup Socket";
  $scope.$parent.pageClass = 'page-contact';
  
  $http.get("services/getap100_setup.php")
    .success(function (response) {
    
      $scope.filesap100Setup = response;
  });

    $scope.addToOrder = function(f)
  {
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = f.filename;
                result['socket_type'] = f.stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var index = -1;    
                var filesArr = eval( $scope.filesap100Setup );
                for( var i = 0; i < filesArr.length; i++ ) 
                  {
                    if( filesArr[i].stype === stype & filesArr[i].filename === filename) 
                    {
                    index = i;
                    break;
                    }
                  }
                  if( index === -1 ) {
                   alert( "Something gone wrong" );
                  }
                  $scope.filesap100Setup.splice( index, 1 ); 
            }); 
          }
  }
  
}]);



app.controller('ScanController', ['$scope', '$location', '$routeParams', '$http', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder','ModalService', function($scope, $location, $routeParams, $http, $filter, DTOptionsBuilder, DTColumnBuilder, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "Scan Socket";
  $scope.$parent.pageClass = 'page-contact';


  $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }

  var lang = $scope.lang;

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/getscan.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle("Drawing No").renderWith(function(data,type,full)
   {
      return data;
   }),
     
   DTColumnBuilder.newColumn('filename').withTitle(lang.filename).renderWith(function(data,type,full)
    {
     return '<a style="margin-left:10px" href= "' + full.href + '" target="_blank" >' + data + '</a><br>';
     
    }),
   DTColumnBuilder.newColumn('cdate').withTitle(lang.date).renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('id').withTitle("Customer").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Material").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Delivery Date").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Quantity").renderWith(function(data,type,full)
    {
      return "";  
    }),
   DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
    var filename = encodeURIComponent(full.filename);
      return '<button class= "imgbtn"' +  'onclick=addToOrder("' + filename + '","' + full.stype + '")' + ' style="padding-left:5px"><img src="images/add.png"></button>' +
            '<button class="imgbtn" onclick=deleteItem("' + filename + '","' + full.stype + '")' +  ' style="padding-left:5px"><img src="images/delete.png"></button>' +
            '<button class="imgbtn" onclick=editScan("' + full.id + '","' + filename + '")' +  ' style="padding-left:5px"><img src="images/pencil-icon.png"></button>';
   })
   ];

   /*$http.get("services/getscan.php")
    .success(function (response) {
    
      $scope.scanfiles = response;
  });*/
  
  $scope.addToOrder = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = filename;
                result['socket_type'] = stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
            }); 
          }
  }

  $scope.editScan = function(id,filename)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "edit_scan.html",
            controller: "EditScanController",
            inputs: {
                filename: filename,
                id : id
            }
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }

                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
                  
            });
        });
  }
  addToOrder = $scope.addToOrder;
  deleteItem = $scope.deleteitem;
  editScan =  $scope.editScan;

}]);

app.controller('RouterSheetController', ['$scope', '$location', '$routeParams', '$http', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder','ModalService', function($scope, $location, $routeParams, $http, $filter, DTOptionsBuilder, DTColumnBuilder, ModalService) {
  
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "Router Sheet Socket";
  $scope.$parent.pageClass = 'page-contact';


  $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }

  var lang = $scope.lang;

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_router.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
   DTColumnBuilder.newColumn('id').withTitle("Drawing No").renderWith(function(data,type,full)
   {
      return data;
   }),
     
   DTColumnBuilder.newColumn('filename').withTitle(lang.filename).renderWith(function(data,type,full)
    {
     return '<a style="margin-left:10px" href= "' + full.href + '" target="_blank" >' + data + '</a><br>';
     
    }),
   DTColumnBuilder.newColumn('cdate').withTitle(lang.date).renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   }),
   DTColumnBuilder.newColumn('size').withTitle(lang.size).renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('id').withTitle("Customer").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Material").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Delivery Date").renderWith(function(data,type,full)
    {
      return "";
    }),
   DTColumnBuilder.newColumn('id').withTitle("Quantity").renderWith(function(data,type,full)
    {
      return "";  
    }),
   DTColumnBuilder.newColumn('id').withTitle(lang.action).renderWith(function(data, type, full) {
    var filename = encodeURIComponent(full.filename);
      return '<button class= "imgbtn"' +  'onclick=addToOrder("' + filename + '","' + full.stype + '")' + ' style="padding-left:5px"><img src="images/add.png"></button>' +
            '<button class="imgbtn" onclick=deleteItem("' + filename + '","' + full.stype + '")' +  ' style="padding-left:5px"><img src="images/delete.png"></button>' +
            '<button class="imgbtn" onclick=editRouter("' + full.id + '","' + filename + '")' +  ' style="padding-left:5px"><img src="images/pencil-icon.png"></button>';
   })
   ];

   /*$http.get("services/getscan.php")
    .success(function (response) {
    
      $scope.scanfiles = response;
  });*/
  
  $scope.addToOrder = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = filename;
                result['socket_type'] = stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    filename = decodeURIComponent(filename);
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
            }); 
          }
  }

  $scope.editRouter = function(id,filename)
  {
    filename = decodeURIComponent(filename);
    ModalService.showModal({
            templateUrl: "edit_socket.html",
            controller: "EditRouterController",
            inputs: {
                filename: filename,
                id : id
            }
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }

                var resetPaging = false;
              $scope.dtInstance.reloadData(callback, resetPaging);
              
              function callback(json) {
                 console.log(json);
              }
                  
            });
        });
  }
  addToOrder = $scope.addToOrder;
  deleteItem = $scope.deleteitem;
  editRouter =  $scope.editRouter;

}]);

app.controller('ErpPorController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
  
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "ERP - POR";
  $scope.$parent.pageClass = 'page-contact';
  
  $http.get("services/geterp_por.php")
    .success(function (response) {
    
      $scope.fileserpPOR = response;
  });

    $scope.addToOrder = function(f)
  {
    ModalService.showModal({
            templateUrl: "add_to_order.html",
            controller: "AddToOrderController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                result['filename'] = f.filename;
                result['socket_type'] = f.stype;
                var request = $http({
                    method: "post",
                    url: 'services/new_order.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
            });
        });
  }

   $scope.deleteitem = function(filename,stype)
  {
    if(confirm("Do you want to delete the item"))
    {
     var request = $http({
                        method: "post",
                        url: 'services/delete_socketitem.php',
                        data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                var index = -1;    
                var filesArr = eval( $scope.fileserpPOR );
                for( var i = 0; i < filesArr.length; i++ ) 
                  {
                    if( filesArr[i].stype === stype & filesArr[i].filename === filename) 
                    {
                    index = i;
                    break;
                    }
                  }
                  if( index === -1 ) {
                   alert( "Something gone wrong" );
                  }
                  $scope.fileserpPOR.splice( index, 1 ); 
            }); 
          }
  }
  
}]);

app.controller('AddToOrderController', ['$scope', '$element', 'close', '$http', 
  function($scope, $element, close, $http) {
  
  $http.get("services/get_vendors.php").success(function (response) {
		$scope.vendors = response;
   });

  $scope.ordername = null;
  $scope.itemsOrderType = [ {id: 1, label: '2D to Q3D'},{id: 2, label: '2D to 3D'}, {id: 3, label: '3D to 4D'}, {id: 4, label: '2D to 4D'}];
   $scope.serviceType = $scope.itemsOrderType[0];
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
    
    if($scope.ordername == null || $scope.ordername == '')
    {
      alert("Enter the order name");
      $("#ordername").focus();
      return;
    }
    
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
      ordername: $scope.ordername,
      service: $scope.serviceType.id,
      comment : $scope.note,
	     vendor: $scope.vendor,
    dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

}]);

app.controller('EditScanController', ['$scope', '$element', 'close', '$http', 'filename', 'id',
  function($scope, $element, close, $http, filename,id) {
  
  $scope.scan_filename = filename;
  $scope.close = function() {
    
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  $scope.save = function()
  {
    var result = {};
    result['filename'] = $scope.scan_filename;
    result['id'] = id;
    result['old_filename'] = filename;

                var request = $http({
                    method: "post",
                    url: 'services/save_scan.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
     $scope.close();
  }

}]);

app.controller('EditRouterController', ['$scope', '$element', 'close', '$http', 'filename', 'id',
  function($scope, $element, close, $http, filename,id) {
  
  $scope.edit_filename = filename;
  $scope.close = function() {
    
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  $scope.save = function()
  {
    var result = {};
    result['filename'] = $scope.edit_filename;
    result['id'] = id;
    result['old_filename'] = filename;

                var request = $http({
                    method: "post",
                    url: 'services/save_router.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
     $scope.close();
  }

}]);

app.controller('EditInspController', ['$scope', '$element', 'close', '$http', 'filename', 'id',
  function($scope, $element, close, $http, filename,id) {
  
  $scope.insp_filename = filename;
  $scope.close = function() {
    
    //  Manually hide the modal.
    $element.modal('hide');
    
    close({
    dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  $scope.save = function()
  {
    var result = {};
    result['filename'] = $scope.insp_filename;
    result['id'] = id;
    result['old_filename'] = filename;

                var request = $http({
                    method: "post",
                    url: 'services/save_insp.php',
                    data: result,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
                
                request.success(function(response) {

                });
     $scope.close();
  }

}]);

app.controller('ADsocDashController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "adsocket Dashboard";
  $scope.$parent.pageClass = 'page-contact';
  
   $http.get("services/adsoc_dashboard.php")
    .success(function (response) {
    var des = angular.fromJson(response);
      $scope.piedata = [{
                          value: response[0].cad2d,
                          color: "#1ac6ff",
                          label: "2D CAD"}, 
                          {
                          value: response[0].cad3d,
                          color: "#66cc00",
                          label: "3D CAD"}, 
                          {
                          value: response[0].scan,
                          color: "#ff8c1a",
                          label: "SCAN"
                      }];

      var pieoptions = {
      segmentShowStroke: false,
      animateRotate: true,
      animateScale: true
      }

      var ctxpie = document.getElementById("pie").getContext("2d");
      var pieChart = new Chart(ctxpie).Pie($scope.piedata,pieoptions);
  });
}]);

app.controller('CameraController', ['$scope', '$location', '$routeParams', '$http','$document', 'ModalService', function($scope, $location, $routeParams, $http,$document, ModalService) {
	
  $scope.$parent.navSelectedItem = "adsocket";
  $scope.$parent.subtitle = "Camera Socket";
  $scope.$parent.pageClass = 'page-contact';

 $scope.size = {"value" : 'List', 
                 "values" : ["List", "Grid"]
                 };

   $scope.imgClass = "col-xs-6 col-sm-4 col-md-3 col-lg-3";
   $scope.imgHeight = "232";
   $scope.imgWidth = "232";

   $scope.borderColor = "#ffffff";

   $scope.double_click = function()
   {
    alert("double click");
   }
   $scope.single_click = function(obj)
   {
    var item = obj.currentTarget;

    var res =  $document.find('#'+item.id); 
    var bool = item.getAttribute("selected");
    //item.remove();
    if(bool === "true")
    {
      res.css("border", "1px ridge #ffffff");
      item.setAttribute("selected","false");
    }
    else
    {
       res.css( "border", "3px ridge #ff6600" );
       item.setAttribute("selected",true);
    }
   }

    $scope.download = function()
     {
       var files = [];
        var res = $document.find('[selected="true"]');
        for (var i = 0; i < res.length; i++)
        {
            console.log(res[i].attributes["src"].nodeValue);
            var path = res[i].attributes["src"].nodeValue;
            var filename = path.replace(/^.*[\\\/]/, '');
            filename = filename.substring(1);
            files.push(filename);
        }
        if(files.length < 1) 
          {
            alert("Please select photos to download");
            return;
          }
        if (!confirm("Are you confirm about download the selected files?")) return;

        var request = $http({
                        method: "post",
                        url: 'services/download.php',
                        data: { files : files},
                        responseType: 'arraybuffer'
            });
            request.success(function(data) {
              var a = document.createElement('a');
              var blob = new Blob([data], {'type':"application/octet-stream"});
              a.href = URL.createObjectURL(blob);
              a.download = "alfaDOCK_images.zip";
              a.click();
            }); 
     }
     $scope.delete = function()
     {
        var id = [];
        var res = $document.find('[selected="true"]');
        for (var i = 0; i < res.length; i++)
        {
            console.log(res[i].id);
            id.push(res[i].id);
        }
        if(id.length < 1) 
          {
            alert("Please select photos to delete");
            return;
          }
        if (!confirm("Are you confirm about deleting the files?")) return;
        res.remove();
         var request = $http({
                        method: "post",
                        url: 'services/delete_photo.php',
                        data: { id : id},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
               $scope.getPhotos();
            }); 

     }
  $scope.getPhotos = function()
  {
    var request = $http({
                        method: "post",
                        url: 'services/get_camera.php',
                        //data: { filename : filename, stype: stype},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            request.success(function(response) {
                $scope.images = response;
            }); 
  }
  $scope.sizeChange = function()
  {
   //alert($scope.size.value);
    var size = $scope.size.value;
    if(size == "Small")
    {
       $scope.imgClass = "col-xs-6 col-sm-4 col-md-6 col-lg-1";
       $scope.imgHeight = "100";
       $scope.imgWidth = "100";
    }else if(size == "Medium")
    {
       $scope.imgClass = "col-xs-6 col-sm-4 col-md-6 col-lg-2";
       $scope.imgHeight = "200";
       $scope.imgWidth = "200";
    }else if(size == "Large")
    {
       $scope.imgClass = "col-xs-6 col-sm-4 col-md-6 col-lg-3";
       $scope.imgHeight = "250";
       $scope.imgWidth = "250";
    }
    
  }
  $scope.getPhotos();

    var dp1 = $('#datetimepicker1').datetimepicker({format: 'MM-DD-YYYY 00:00:00'});
    var dp2 = $('#datetimepicker2').datetimepicker({format: 'MM-DD-YYYY 23:55:55'});
    dp1.on("dp.change", function(e) {
            var sdate = $("#datetimepicker1").find("input").val();
            var edate = $("#datetimepicker2").find("input").val();
            
            sdate = new Date(sdate);
            edate = new Date(edate);
            
        });
    dp2.on("dp.change", function(e) {
        });
  
}]);

var deleteRow;

/*app.controller('SubContractOrdersController', ['$scope', '$http', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', 
    function($scope, $http, $filter, DTOptionsBuilder, DTColumnBuilder) {

    $scope.$parent.navSelectedItem = "orders";
    $scope.$parent.subtitle = $scope.lang.orders;
    $scope.pageClass = 'page-contact';
    
    window.scrollTo(0, 0);
    
    $scope.dtInstance = {};
    
    $scope.deleteRow = function(id) 
    { 
        if (confirm("Do you want to delete the order - " + id + " ?")) {
        // todo code for deletion
    
    var request = $http({
        method: "post",
        url: 'services/remove_order.php',
        data: { orderid : id },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      });
      request.success(function(response) {
            // success - reload table
            var resetPaging = false;
            $scope.dtInstance.reloadData(callback, resetPaging);
            
            function callback(json) {
               console.log(json);
            }
        
      }); 
        }
  };
    
    deleteRow = $scope.deleteRow;
    
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: 'services/get_contract_orders.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc']);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('orderno').withTitle('Order no'),
        DTColumnBuilder.newColumn('orderdate').withTitle('Order Date').renderWith(function(data, type, full) {
            return $filter('date')($scope.parseDate(data), 'yyyy/M/dd'); //date filter $scope.parseDate(data) | date:'yyyy/M/dd';
        }),
        DTColumnBuilder.newColumn('ordername').withTitle('Order Name'),
        DTColumnBuilder.newColumn('status').withTitle('Order Status').renderWith(function(data, type, full) {
            
            return data[data.length - 1].status;
           
        }),
        DTColumnBuilder.newColumn('inputfiles').withTitle('Input Files').renderWith(function(data, type, full) {
            var ret = "";
            for(var i = 0; i < data.length; i++) {
                var f = data[i];
                ret = ret + "<a href= " + f.href + " download >" + f.filename + "</a><br>"; 
            }
            
            return ret;
        }),
        DTColumnBuilder.newColumn('ddfiles').withTitle('DD Files').renderWith(function(data, type, full) {
            var ret = "";
            for(var i = 0; i < data.length; i++) {
                var f = data[i];
                ret = ret + "<a href= " + f.href + " download >" + f.filename + "</a><br>"; 
            }
            
            return ret;
        }),
        DTColumnBuilder.newColumn('orderno').withTitle('Action').renderWith(function(data, type, full) {
            return '<a href="#edit/' + data + '"><img src="images/pencil-icon.png"><i class="images/pencil-icon.png"></i></a>' + 
           '<a onclick="deleteRow(' + data + ')"><img src="images/minus-icon.png"></a>';
        }),
        //DTColumnBuilder.newColumn('lastName').withTitle('Last name').notVisible()
    ];
    
    $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
    
    $scope.fileLink = function(filePath)
    {
        return "<a href =''>file1</a>";
    }
    
    
}]);*/

app.controller('AirportController', ['$scope', '$filter', '$http',
  function($scope, $filter, $http) {
     $scope.$parent.navSelectedItem = "airport";
     $scope.$parent.subtitle = "Airport";
     $scope.$parent.pageClass = 'page-contact';
  
}]);

app.controller('PORController', ['$scope', '$filter', '$http',
  function($scope, $filter, $http) {
     $scope.$parent.navSelectedItem = "POR";
     $scope.$parent.subtitle = "POR";
     $scope.$parent.pageClass = 'page-about';

  $http({
        method: 'POST',
        url: 'services/get_por.php'
        }).then(function successCallback(response) {
           
           $scope.por_orders = response.data;
        });
}]);

app.controller('RODController', ['$scope', '$filter', '$http',
  function($scope, $filter, $http) {
     $scope.$parent.navSelectedItem = "ROD";
     $scope.$parent.subtitle = "ROD";
     $scope.$parent.pageClass = 'page-about';

  $http({
        method: 'POST',
        url: 'services/get_rod.php'
        }).then(function successCallback(response) {
           
           $scope.rod_orders = response.data;
        });
}]);

app.controller('SharedController', ['$scope', '$filter', '$http',
  function($scope, $filter, $http) {

    $scope.$parent.navSelectedItem = "Shared";
     $scope.$parent.subtitle = "Shared";
     $scope.$parent.pageClass = 'page-about';

    $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        $scope.customer = response.data;
        if($scope.customer.id == 1)
        {
          $scope.shared_orders = "#airport_shared_orders";
          $scope.shared_office = "#airport_shared_office";
        } 
        else if($scope.customer.id == 8)
        {
            $scope.shared_orders = "#j_airport_shared_orders";
            $scope.shared_office = "#j_airport_shared_office";
        }
        else if($scope.customer.id == 22)
        {
            $scope.shared_orders = "#simy_shared_orders";
            $scope.shared_office = "#simy_shared_office";
        }
        else if($scope.customer.id == 3)
        {
            $scope.shared_orders = "#nishiyama_shared_orders";
            $scope.shared_office = "#nishiyama_shared_office";
        }
        else
          {
            $scope.shared_orders = "#shared";
            $scope.shared_office = "#shared";
          }
      });

}]);

app.controller('AirportSharedOrdersController', ['$scope', '$filter', '$http',
  function($scope, $filter, $http) {

    $scope.$parent.navSelectedItem = "Shared Orders";
    $scope.$parent.subtitle = "Shared Orders";
    $scope.pageClass = 'page-contact';

}]);

app.controller('AirportSharedOfficeController', ['$scope', '$filter', '$http',
  function($scope, $filter, $http) {

    $scope.$parent.navSelectedItem = "Shared Office";
    $scope.$parent.subtitle = "Shared Office";
    $scope.pageClass = 'page-contact';

}]);


app.controller('SharedOrdersListController', ['$scope', '$filter', '$http','$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder',
  function($scope, $filter, $http,$routeParams,DTOptionsBuilder, DTColumnBuilder) {

    $scope.$parent.navSelectedItem = "Shared Orders";
    $scope.$parent.subtitle = "Shared Orders";
    $scope.pageClass = 'page-contact';
    
    var cust_id = $routeParams.id;
        
    window.scrollTo(0, 0);
    
    $scope.dtInstance = {};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         data: {id : cust_id},
         url: 'services/get_shared_orders.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc']);
    $scope.dtColumns = [

        DTColumnBuilder.newColumn('orderno').withTitle('Order no'),
        DTColumnBuilder.newColumn('orderdate').withTitle('Order Date').renderWith(function(data, type, full) {
            return $filter('date')($scope.parseDate(data), 'yyyy/M/dd'); //date filter $scope.parseDate(data) | date:'yyyy/M/dd';
        }),
        DTColumnBuilder.newColumn('ordername').withTitle('Order Name'),
        DTColumnBuilder.newColumn('status').withTitle('Order Status').renderWith(function(data, type, full) {
            return data[data.length - 1].status;
        }),
        DTColumnBuilder.newColumn('inputfiles').withTitle('Input Files').renderWith(function(data, type, full) {
            var ret = "";
            for(var i = 0; i < data.length; i++) {
                var f = data[i];
                ret = ret + "<a href= " + f.href + " download >" + f.filename + "</a><br>"; 
            }
            
            return ret;
        }),
        DTColumnBuilder.newColumn('ddfiles').withTitle('DD Files').renderWith(function(data, type, full) {
            var ret = "";
            for(var i = 0; i < data.length; i++) {
                var f = data[i];
                ret = ret + "<a href= " + f.href + " download >" + f.filename + "</a><br>"; 
            }
            
            return ret;
        }),
         DTColumnBuilder.newColumn('orderno').withTitle('Action').renderWith(function(data, type, full) {
            return '<a href="#edit/' + data + '"><img src="images/pencil-icon.png"><i class="images/pencil-icon.png"></i></a>';
        }),
    ];
    
    /*$http.get("services/get_orders.php")
    .success(function (response) {
    
    $scope.orders = response;});*/
    
    $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
    
    $scope.fileLink = function(filePath)
    {
        return "<a href =''>file1</a>";
    }
  
}]);

app.controller('SharedOfficeController', ['$scope', '$location', '$routeParams', '$http', '$filter','DTOptionsBuilder', 'DTColumnBuilder','ModalService', 
  function($scope, $location, $routeParams, $http, $filter,DTOptionsBuilder, DTColumnBuilder, ModalService) {
  
  $scope.$parent.navSelectedItem = "Shared Office";
  $scope.$parent.subtitle = "Shared Office";
  $scope.$parent.pageClass = 'page-about';

  var cust_id = $routeParams.id;
        

  $scope.checkedID = [];  //Checked checkboxes
  $scope.dtInstance = {};
  //$("#btnRemove").attr("disabled", true);
  //$("#btnAddToOrder").attr("disabled", true);

  $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         data: {id : cust_id},
         url: 'services/get_shared_office.php',
         type: 'POST'
     })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('order', [1, 'desc']);
       
   $scope.dtColumns = [
  
   DTColumnBuilder.newColumn('id').withTitle('ID'),
     
   DTColumnBuilder.newColumn('filename').withTitle('FileName').renderWith(function(data,type,full)
    {
      return "<a href= " + full.href + " download >" + data + "</a><br>";
    }),
   DTColumnBuilder.newColumn('type').withTitle('FileType'),
   DTColumnBuilder.newColumn('size').withTitle('FileSize').renderWith(function(data,type,full)
   {
      return data + " MB";
   }),
   DTColumnBuilder.newColumn('date').withTitle('Date').renderWith(function(data,type,full)
   {
      var pdate = $scope.parseDate(data);
      var date = $filter('date')(pdate, 'yyyy/M/dd');
      return date;
   })
   ];

   $scope.parseDate = function(dtStr)
    {
        var dateTime = dtStr.split(/[\s-]+/); // split by space and - yyyy-m-dd HH:mm:ss
        return new Date(dateTime[0], dateTime[1] - 1, dateTime[2]); // months are indexed in js
    }
        
}]);


app.controller('AirportSettingsController', ['$scope', '$filter', '$http','ModalService',
  function($scope,$filter, $http,ModalService) {

    $scope.$parent.navSelectedItem = "AirportSettings";
    $scope.$parent.subtitle = "Airport Settings";
    $scope.$parent.pageClass = 'page-about';

  $http({
        method: 'POST',
        url: 'services/get_vendors.php'
        }).then(function successCallback(response) {
           
           $scope.vendors = response.data;
        });

  $http({
        method: 'POST',
        url: 'services/get_clients.php'
        }).then(function successCallback(response) {
           
           $scope.clients = response.data;
        });

   $scope.addVendorDialog = function()
         {
            ModalService.showModal({
            templateUrl: "add_vendor.html",
            controller: "AddVendorController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                $http({
                    method: 'POST',
                    url: 'services/get_vendors.php'
                    }).then(function successCallback(response) {
                       
                       $scope.vendors = response.data;
                    });
                
            });
        });
         }

         $scope.addClientDialog = function()
         {
            ModalService.showModal({
            templateUrl: "add_client.html",
            controller: "AddClientController",
        
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                
                if(result.dialogResult == 0) 
                {
                    return;
                }
                $http({
                    method: 'POST',
                    url: 'services/get_clients.php'
                    }).then(function successCallback(response) {
                       
                       $scope.clients = response.data;
                    });
                
            });
        });
         }
}]);

app.controller('AddVendorController', ['$scope', '$element','$filter', '$http','close', 
  function($scope, $element,$filter, $http, close) {

  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
      close({
      dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

var request = $http({
                        method: "post",
                        url: 'services/get_all_customers.php',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });
              request.success(function(response) {
                $scope.vendors = response;
              }); 

    $scope.selectUpdate = function()
    {
      $('#email').val($scope.vendor.email);
    }

   $scope.addVendor = function() {
                
            var checkNull = $scope.checkNull;

            if(checkNull("vendor_name") && checkNull("email") && checkNull("username") && checkNull("password"))
            {
              var vendor_name = $scope.vendor.name;
              var address = $('#address').val();
              var email = $('#email').val();
              var username = $('#username').val();
              var password = $('#password').val();
              var vendor_id = $scope.vendor.id;

              var request = $http({
                        method: "post",
                        url: 'services/add_vendor.php',
                        data: {name : vendor_name, vendor_id: vendor_id, address : address, email: email, username : username, password : password},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });
              request.success(function(response) {
                $scope.close(); 
              }); 
            }
            
        }; 

       $scope.checkNull = function(name)
        {
          value = $("#"+name).val();
            if(value != '') return true;
            else {
              alert("Please enter " + name);
              return false;
            }
        } 


}]);

app.controller('AddClientController', ['$scope', '$element','$filter', '$http','close', 
  function($scope, $element,$filter, $http, close) {

  $scope.close = function() {
    //  Manually hide the modal.
    $element.modal('hide');
    
      close({
      dialogResult: 1
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Now call close, returning control to the caller.
    close({
      dialogResult: 0
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  $scope.addClient = function() {
              
            var checkNull = $scope.checkNull;

            if(checkNull("username") && checkNull("password") && checkNull("domain"))
            {
              var username = $('#username').val();
              var password = $('#password').val();
              var domain = $('#domain').val();
              var request = $http({
                        method: "post",
                        url: 'services/add_client.php',
                        data: {username : username, password : password, domain : domain},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });
              request.success(function(response) {

                var result = response;
                $scope.close(); 
                
              }); 
            }
            
        }; 

        $scope.checkNull = function(name)
        {
          value = $("#"+name).val();
            if(value != '') return true;
            else {
              alert("Please enter " + name);
              return false;
            }
        } 

}]);



