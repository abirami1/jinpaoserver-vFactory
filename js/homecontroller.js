﻿var app = angular.module('ProjectTrackingModule');

app.controller('HomeController', ['$scope', '$location', '$routeParams', '$http', 'ModalService', function($scope, $location, $routeParams, $http, ModalService) {
	
  $scope.$parent.navSelectedItem = "home";
  $scope.$parent.subtitle = "Home";
  $scope.pageClass = 'page-home';
  
  var lang = $scope.lang;

  $http({
        method: 'POST',
        url: 'services/get_cust.php'
        }).then(function successCallback(response) {
        
        $scope.customer = response.data;
        var custid = $scope.customer.id;
        if(custid == 5)
          $scope.adsocket = "#yoshimi_adsocket";
        else if(custid == 1)
          $scope.adsocket = "#hirano_adsocket";
        else if(custid == 10)
          $scope.adsocket = "#sakae_adsocket";
        else 
          $scope.adsocket = "#adsocket";
      });
  
  $http.get("services/getFactory.php")
    .success(function (response) {
    
      $scope.factory = response;

      if($scope.factory.length > 0)
      {
      $scope.userErp = $scope.customer.username + " " + lang.factory;
      $scope.erpLink = "#factory";
      switch($scope.customer.id)
      {
        case "1": $scope.erpLink = "#hirano_factory"; 
                  break;
        case "2": $scope.erpLink = "#sasaki_factory"; break;
        case "3": $scope.erpLink = "#nishiyama_factory"; break;
        case "4": $scope.erpLink = "#atkg_factory"; 
                  $scope.userErp = "alfaTKG Factory";
                  break;
        case "5": $scope.erpLink = "#yoshimi_factory"; 
                  break;
        case "7": $scope.erpLink = "#atkg_factory"; 
                  $scope.userErp = "alfaTKG Factory";
                  break;
        case "8": $scope.erpLink = "#jinpao_factory"; 
                  $scope.userErp = "Jinpao Factory";
                  break;
         case "10": $scope.erpLink = "#sakae_factory"; 
                  $scope.userErp = "Sakae Factory";
                  break;
        case "11": $scope.erpLink = "#tsukata_factory"; 
                  $scope.userErp = "Tsukata Factory";
                  break;
        case "13": $scope.erpLink = "#mori_factory"; 
                  $scope.userErp = "Moriseiko Factory";
                  break;
        case "16" : $scope.erpLink = "#kimpou_factory"; 
                   $scope.userErp = "Kimpou Factory";
                   break;
       case "18" : $scope.erpLink = "#usui_factory"; 
                   $scope.userErp = "Usui Factory";
                   break;
       case "20" : $scope.erpLink = "#washin_factory"; 
                   $scope.userErp = "Washin Factory";
                   break;
        case "21" : $scope.erpLink = "#horii_factory"; 
                   $scope.userErp = "Horii Factory";
                   break;

        case "22" : $scope.erpLink = "#simy_factory"; 
                   $scope.userErp = "NY-Myanmar Factory";
                   break;
        case "24" : $scope.erpLink = "#hoshiba_factory"; 
                   $scope.userErp = "株式会社 ホシバ Factory";
                   break;
        case "25" : $scope.erpLink = "#okabe_factory"; 
                   $scope.userErp = "Okabe Factory";
                   break;
      	case "26" : $scope.erpLink = "#makino_factory"; 
                         $scope.userErp = "Makino Factory";
                         break;
      	case "27": $scope.erpLink = "#mori_factory"; 
                        $scope.userErp = "Moriseiko Factory";
                        break;
        case "28": $scope.erpLink = "#sacs_factory"; 
                        $scope.userErp = "SACS Factory";
                        break;
      }
      
      $scope.erpImg = "factory_layout/"+$scope.customer.id +"/logo.png";
      }

      else
      {
        $scope.userErp = $scope.customer.username + " " + lang.factory;
        $scope.erpLink = "#factory";
        $scope.erpImg = "images/erp.png";
      }

    });
  
}]);